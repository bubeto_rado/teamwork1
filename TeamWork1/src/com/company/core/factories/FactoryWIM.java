package com.company.core.factories;


import com.company.models.common.PriorityType;
import com.company.models.common.SeverityType;
import com.company.models.common.SizeType;
import com.company.models.common.StatusType;
import com.company.models.WorkItems.contractsWorkItem.Bug;
import com.company.models.WorkItems.contractsWorkItem.Feedback;
import com.company.models.WorkItems.contractsWorkItem.Story;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;

import java.util.List;

public interface FactoryWIM {
    Bug createBug(String title, String description, String priority, List<String> stepsToReproduce, String severity);

    Story createStory(String title, String description, String priority);

    Feedback createFeedback(String title, String description, int rating);

    Team createTeam(String name);

    Member createMember(String name);

    StatusType getStatus(String status);

    PriorityType getPriority(String priority);

    SeverityType getSeverity (String severity);

    SizeType getSize(String size);

}
