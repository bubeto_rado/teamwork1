package com.company.core.factories;

import com.company.models.common.PriorityType;
import com.company.models.common.SeverityType;
import com.company.models.common.SizeType;
import com.company.models.common.StatusType;
import com.company.models.MemberImpl;
import com.company.models.TeamImpl;
import com.company.models.WorkItems.BugImpl;
import com.company.models.WorkItems.FeedbackImpl;
import com.company.models.WorkItems.StoryImpl;
import com.company.models.WorkItems.contractsWorkItem.Bug;
import com.company.models.WorkItems.contractsWorkItem.Feedback;
import com.company.models.WorkItems.contractsWorkItem.Story;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;

import java.util.List;

public class FactoryWIMImpl implements FactoryWIM {

    @Override
    public StatusType getStatus(String status) {
        switch (status.toLowerCase()) {
            case "active":
                return StatusType.ACTIVE;
            case "fixed":
                return StatusType.FIXED;
            case "new":
                return StatusType.NEW;
            case "unscheduled":
                return StatusType.UNSCHEDULED;
            case "scheduled":
                return StatusType.SCHEDULED;
            case "done":
                return StatusType.DONE;
            case "notdone":
                return StatusType.NOT_DONE;
            case "inprogress":
                return StatusType.IN_PROGRESS;
            default:
                throw new IllegalArgumentException("Invalid status type!");
        }
    }

    @Override
    public PriorityType getPriority(String priority) {
        switch (priority.toLowerCase()) {
            case "high":
                return PriorityType.HIGH;
            case "medium":
                return PriorityType.MEDIUM;
            case "low":
                return PriorityType.LOW;
            default:
                throw new IllegalArgumentException("Invalid priority type!");
        }
    }

    @Override
    public SeverityType getSeverity(String severity) {
        switch (severity.toLowerCase()) {
            case "critical":
                return SeverityType.CRITICAL;
            case "major":
                return SeverityType.MAJOR;
            case "minor":
                return SeverityType.MINOR;
            default:
                throw new IllegalArgumentException("Invalid severity type!");
        }
    }

    @Override
    public SizeType getSize(String size) {
        switch (size.toLowerCase()) {
            case "large":
                return SizeType.LARGE;
            case "medium":
                return SizeType.MEDIUM;
            case "small":
                return SizeType.SMALL;
            default:
                throw new IllegalArgumentException("Invalid size type!");
        }
    }

    @Override
    public Bug createBug(String title, String description, String priority, List<String> stepsToReproduce, String severity) {
        return new BugImpl(title, description, getPriority(priority), stepsToReproduce, getSeverity(severity));
    }

    @Override
    public Story createStory(String title, String description, String priority) {
        return new StoryImpl(title, description, getPriority(priority));
    }

    @Override
    public Feedback createFeedback(String title, String description, int rating) {
        return new FeedbackImpl(title, description, rating);
    }

    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Member createMember(String name) {
        return new MemberImpl(name);
    }
}
