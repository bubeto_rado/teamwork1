package com.company.core;


import com.company.models.WorkItems.contractsWorkItem.WorkItem;
import com.company.commands.contracts.Command;
import com.company.models.contracts.*;
import com.company.core.contracts.*;
import com.company.core.factories.*;
import com.company.core.providers.*;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EngineImpl implements Engine {
    private static final String TERMINATION_COMMAND = "Exit";

    private Reader reader;
    private Writer writer;
    private Parser parser;
    private final Map<Integer, WorkItem> workItems;
    private final Map<String, Member> members;
    private final Map<String, Team> teams;


    public EngineImpl(FactoryWIM factory) {
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        parser = new CommandParser(factory, this);

        workItems = new HashMap<>();
        members = new HashMap<>();
        teams = new HashMap<>();

    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);
            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
                //writer.writeLine("####################");
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }
        Command command = parser.parseCommand(commandAsString);
        Map<String,String> parameters = parser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
        //writer.writeLine("####################");
    }

    @Override
    public Map<Integer, WorkItem> getWorkItems() {
        return workItems;
    }

    @Override
    public Map<String, Member> getMembers() {
        return members;
    }

    @Override
    public Map<String, Team> getTeams() {
        return teams;
    }
}
