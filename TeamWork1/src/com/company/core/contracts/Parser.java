package com.company.core.contracts;

import com.company.commands.contracts.Command;

import java.util.List;
import java.util.Map;

public interface Parser {
    Command parseCommand(String fullCommand);

    Map<String, String> parseParameters(String fullCommand);
}
