package com.company.core.contracts;



import com.company.models.contracts.Member;
import com.company.models.WorkItems.contractsWorkItem.WorkItem;
import com.company.models.contracts.Team;

import java.util.Map;

public interface Engine {
    void start();

   Map<Integer, WorkItem> getWorkItems();
   Map<String , Member> getMembers();
   Map<String , Team> getTeams();
}
