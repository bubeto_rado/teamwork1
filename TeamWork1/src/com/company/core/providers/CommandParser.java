package com.company.core.providers;


import com.company.commands.contracts.AddCommand;
import com.company.commands.contracts.Command;
import com.company.commands.createCommands.*;

import com.company.commands.showCommands.AssignCommand;
import com.company.commands.showCommands.ListCommand;
import com.company.commands.showCommands.ShowCommand;
import com.company.commands.showCommands.UnassignCommand;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Parser;
import com.company.core.factories.FactoryWIM;

import java.util.*;


public class CommandParser implements Parser {
    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    private final FactoryWIM factory;
    private final Engine engine;

    public CommandParser(FactoryWIM factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public Command parseCommand(String fullCommand) {
        String commandName = fullCommand.split(" ")[0];
        return findCommand(commandName);
    }

    public Map<String, String> parseParameters(String fullCommand) {
//        fullCommand = "-" + fullCommand;
        String[] commandParts = fullCommand.split("-");
        Map<String, String> parameters = new HashMap<>();
        for (int i = 1; i < commandParts.length; i++) {
            String key = commandParts[i].split(" ")[0].trim();
            String value = commandParts[i].substring(key.length()).trim();
            parameters.put(key, value);
        }
        return parameters;
    }
//commands ! ! !

    private Command findCommand(String commandName) {
        switch (commandName.toLowerCase()) {

            case "create":
                return new CreateCommand(engine, factory);
            case "show":
                return new ShowCommand(engine);
            case "add":
                return new AddCommand(engine);
            case "change":
                return new ChangeCommand(engine, factory);
            case "assign":
                return new AssignCommand(engine);
            case "unassign":
                return new UnassignCommand(engine);
            case "list":
                return new ListCommand(engine, factory);

        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandName));
    }
}
