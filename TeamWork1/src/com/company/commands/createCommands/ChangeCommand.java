package com.company.commands.createCommands;

import com.company.commands.CommandBase;
import com.company.core.contracts.Engine;
import com.company.core.factories.FactoryWIM;
import com.company.models.WorkItems.contractsWorkItem.Bug;
import com.company.models.WorkItems.contractsWorkItem.Feedback;
import com.company.models.WorkItems.contractsWorkItem.Story;
import com.company.models.WorkItems.contractsWorkItem.WorkItem;

import java.util.Map;

public class ChangeCommand extends CommandBase {
    public static final String SUCCESSFULLY_CHANGED_PROPERTY = "%s for %s with ID: %d was successfully changed to %s!";
    private final FactoryWIM factoryWIM;
    public ChangeCommand(Engine engine, FactoryWIM factoryWIM) {
        super(engine);
        this.factoryWIM = factoryWIM;
    }

    @Override
    public String execute(Map<String, String> parameters) {
        if(parameters.size() != 2) {
            throw new IllegalArgumentException("This command only accepts two options!");
        }
        if(!parameters.containsKey("workitem")) {
            throw new IllegalArgumentException("You must specify work item id!");
        }
        int ID = 0;
        try {
            ID = Integer.parseInt(parameters.get("workitem"));
        } catch (Exception e) {
            throw new IllegalArgumentException("ID must be integer");
        }
        WorkItem workItem = findWorkItem(ID);
        if(parameters.containsKey("severity")) {
            if(!isBug(workItem)) {
                throw new IllegalArgumentException("Severity can only be changed for bugs!");
            }
            Bug bug = (Bug) workItem;
            bug.changeSeverity(factoryWIM.getSeverity(parameters.get("severity")));
            return String.format(SUCCESSFULLY_CHANGED_PROPERTY,"Severity","bug", bug.getID(), bug.getSeverity());
        }

        if (parameters.containsKey("priority")){
            if(isBug(workItem)){
                Bug bug = (Bug) workItem;
                bug.changePriority(factoryWIM.getPriority(parameters.get("priority")));
                return String.format(SUCCESSFULLY_CHANGED_PROPERTY,"Priority","bug",bug.getID(),bug.getPriority());
            }else if (isStory(workItem)){
                Story story = (Story) workItem;
                story.changePriority(factoryWIM.getPriority(parameters.get("priority")));
                return String.format(SUCCESSFULLY_CHANGED_PROPERTY,"Priority","story",story.getID(),story.getPriority());
            }else{
                throw new IllegalArgumentException("No priority for this work item!");
            }
        }
        if (parameters.containsKey("status")){
            if(isBug(workItem)){
                Bug bug = (Bug) workItem;
                bug.changeStatus(factoryWIM.getStatus(parameters.get("status")));
                return String.format(SUCCESSFULLY_CHANGED_PROPERTY,"Status","bug",bug.getID(),bug.getStatus());
            }
            if (isStory(workItem)){
                Story story = (Story) workItem;
                story.changeStatus(factoryWIM.getStatus(parameters.get("status")));
                return String.format(SUCCESSFULLY_CHANGED_PROPERTY,"Status","story",story.getID(),story.getStatus());
            }
            if (isFeedback(workItem)){
                Feedback feedback = (Feedback) workItem;
                feedback.changeStatus(factoryWIM.getStatus(parameters.get("status")));
                return String.format(SUCCESSFULLY_CHANGED_PROPERTY,"Status","feedback",feedback.getID(),feedback.getStatus());
            }
            throw new IllegalArgumentException("Not valid status value!");

        }
        if (parameters.containsKey("size")){
            if(isStory(workItem)){
                Story story = (Story) workItem;
                story.changeSize(factoryWIM.getSize(parameters.get("size")));
                return String.format(SUCCESSFULLY_CHANGED_PROPERTY,"Size","story",story.getID(),story.getSize());
            }
        }
        if (parameters.containsKey("rating")){
            if(!isFeedback(workItem)){
                throw new IllegalArgumentException("Only feedback have rating!");
            }
            Feedback feedback = (Feedback) workItem;
            int rating =1;
            try{
                rating = Integer.parseInt(parameters.get("rating"));
            }catch (Exception e){
                throw new IllegalArgumentException("Rating must be integer!");
            }
            feedback.changeRating(rating);
            return String.format(SUCCESSFULLY_CHANGED_PROPERTY,"Rating","feedback",feedback.getID(),feedback.getRating());
        }

        throw new IllegalArgumentException("Invalid input data!");
    }

    private boolean isBug(WorkItem workItem) {
        return workItem.getClass().getSimpleName().equals("BugImpl");
    }
    private boolean isStory(WorkItem workItem) {
        return workItem.getClass().getSimpleName().equals("StoryImpl");
    }
    private boolean isFeedback(WorkItem workItem) {
        return workItem.getClass().getSimpleName().equals("FeedbackImpl");
    }
}
