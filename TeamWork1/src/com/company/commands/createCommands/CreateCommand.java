package com.company.commands.createCommands;

import com.company.commands.CommandBase;
import com.company.core.contracts.Engine;
import com.company.core.factories.FactoryWIM;
import com.company.models.WorkItems.contractsWorkItem.Bug;
import com.company.models.WorkItems.contractsWorkItem.Feedback;
import com.company.models.WorkItems.contractsWorkItem.Story;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CreateCommand extends CommandBase {
    public static final String SUCCESSFULLY_CREATED = "%s with title: %s was successfully created!";
    private final FactoryWIM factoryWIM;

    public CreateCommand(Engine engine, FactoryWIM factoryWIM) {
        super(engine);
        this.factoryWIM = factoryWIM;
    }

    private String createPerson(String name) {
        if (memberExists(name)) {
            throw new IllegalArgumentException(String.format("Person with name %s already exists!", name));
        }
        Member member = factoryWIM.createMember(name);
        getEngine().getMembers().put(name, member);
        return String.format("Person with name %s was created successfully.", name);
    }

    private String createTeam(String name) {
        if (teamExists(name)) {
            throw new IllegalArgumentException(String.format("Team with name %s already exists!", name));
        }
        if (name.equals("")){
            throw new IllegalArgumentException("Team must have a valid name!");
        }
        Team team = factoryWIM.createTeam(name);
        getEngine().getTeams().put(name, team);
        return String.format("Team with name %s was created successfully.", name);
    }

    private String createBoard(String teamName, String boardName) {
        if (boardExists(teamName, boardName)) {
            throw new IllegalArgumentException(String.format("Board with name %s already exists in team %s!", boardName, teamName));
        }
        Team team = findTeam(teamName);
        team.addBoard(boardName);
        return String.format("Board with name %s was created successfully in team %s.", boardName, teamName);
    }

    private String createBug(Board board, Map<String, String> parametersBug) {
        List<String> steps = new ArrayList<>();
        String priority = "";
        String severity = "";
        if (parametersBug.containsKey("steps")) {
            steps.addAll(Arrays.asList(parametersBug.get("steps").split("#")));
        }
        if (parametersBug.containsKey("priority")) {
            priority = parametersBug.get("priority");
        } else {
            priority = "Low";
        }
        if (parametersBug.containsKey("severity")) {
            severity = parametersBug.get("severity");
        } else {
            severity = "Minor";
        }
        Bug bug = factoryWIM.createBug(parametersBug.get("title"), parametersBug.get("description"), priority, steps, severity);
        getEngine().getWorkItems().put(bug.getID(), bug);
        board.addWorkItem(bug);
        return String.format(SUCCESSFULLY_CREATED, "Bug", bug.getTitle());
    }

    private String createStory(Board board, Map<String, String> parametersStory) {
        String priority = "";
        if (parametersStory.containsKey("priority")) {
            priority = parametersStory.get("priority");
        } else {
            priority = "Low";
        }
        Story story = factoryWIM.createStory(parametersStory.get("title"), parametersStory.get("description"), priority);
        getEngine().getWorkItems().put(story.getID(), story);
        board.addWorkItem(story);
        return String.format(SUCCESSFULLY_CREATED, "Story", story.getTitle());
    }

    private String createFeedback(Board board, Map<String, String> parameteresFeedback) {
        int rating = 1;
        if (parameteresFeedback.containsKey("rating")) {
            try {
                rating = Integer.parseInt(parameteresFeedback.get("rating"));
            } catch (Exception e){
                throw new IllegalArgumentException("Rating must be an integer!");
            }
        }
        Feedback feedback = factoryWIM.createFeedback(parameteresFeedback.get("title"),parameteresFeedback.get("description"),rating);
        getEngine().getWorkItems().put(feedback.getID(), feedback);
        board.addWorkItem(feedback);
        return String.format(SUCCESSFULLY_CREATED,"Feedback",feedback.getTitle());
    }

    private String createWorkItem(Map<String, String> parameters) {
        if (!(parameters.containsKey("bug") || parameters.containsKey("story") || parameters.containsKey("feedback"))) {
            throw new IllegalArgumentException("Work item type not found!");
        }
        if (!parameters.containsKey("title") || !parameters.containsKey("description") || !parameters.containsKey("board")) {
            throw new IllegalArgumentException("Code ninja demands that you provide title, board and description !88");
        }
        String[] parts = parseOption(parameters.get("board"),2);
        Board board = findBoard(parts[0] , parts[1]);
        if (parameters.containsKey("bug")) {
            return createBug(board,parameters);
        }
        if (parameters.containsKey("story")) {
            return createStory(board,parameters);
        }
        if (parameters.containsKey("feedback")){
            return createFeedback(board, parameters);
        }
        throw new IllegalArgumentException("Not valid input!");
    }

    @Override
    public String execute(Map<String, String> parameters) {
        if (parameters.containsKey("bug") || parameters.containsKey("story") || parameters.containsKey("feedback")) {
            return createWorkItem(parameters);
        }
        if (parameters.containsKey("person")) {
            return createPerson(parameters.get("person"));
        }
        if (parameters.containsKey("team")) {
            return createTeam(parameters.get("team"));
        }
        if (parameters.containsKey("board")) {
            String[] parts = parseOption(parameters.get("board"), 2);
            return createBoard(parts[0], parts[1]);
        }
        throw new IllegalArgumentException("Invalid option for create command!");
    }
}
