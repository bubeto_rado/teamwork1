package com.company.commands.contracts;

import com.company.commands.CommandBase;
import com.company.core.contracts.Engine;
import com.company.models.WorkItems.contractsWorkItem.WorkItem;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;

import java.util.Map;

public class AddCommand extends CommandBase {
    public AddCommand(Engine engine) {
        super(engine);
    }

    @Override
    public String execute(Map<String, String> parameters) {
        if (parameters.containsKey("person")) {
            String[] parts = parseOption(parameters.get("person"), 2);
            Team team = findTeam(parts[1]);
            Member member = findMember(parts[0]);
            team.addMember(member);
            return String.format("%s added successfully to team %s!", member.getName(), team.getName());
        }
        if (parameters.containsKey("comment")) {
            String[] parts = parseOption(parameters.get("comment"), 3);
            int ID = 0;
            try {
                ID = Integer.parseInt(parts[0]);
            }catch (Exception e){
                throw new IllegalArgumentException("ID must be integer!");
            }
            WorkItem workItem = findWorkItem(ID);
            workItem.addComment(parts[1], parts[2]);
            return String.format("Comment was added to work item with ID: %d successfully!", workItem.getID());
        }

        throw new IllegalArgumentException("Not valid input!");
    }
}
