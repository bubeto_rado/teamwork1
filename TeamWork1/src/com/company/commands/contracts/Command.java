package com.company.commands.contracts;

import java.util.List;
import java.util.Map;

public interface Command {
    String execute(Map<String,String> parameters);
}
