package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.Engine;
import com.company.models.WorkItems.contractsWorkItem.WorkItem;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;

import java.util.List;

public abstract class CommandBase implements Command {

    private final Engine engine;

    public CommandBase(Engine engine) {
        this.engine = engine;
    }

    public Engine getEngine() {
        return engine;
    }

    protected boolean memberExists(String name) {
        return engine.getMembers().containsKey(name);
    }

    protected Member findMember(String name) {
        if (memberExists(name)) {
            return engine.getMembers().get(name);
        } else {
            throw new IllegalArgumentException(String.format("Person with name %s was not found!", name));
        }
    }

    protected boolean teamExists(String name) {
        return engine.getTeams().containsKey(name);
    }

    protected Team findTeam(String name) {
        if (!teamExists(name)) {
            throw new IllegalArgumentException(String.format("Team with name %s was not found!", name));
        }
        return engine.getTeams().get(name);
    }

    protected boolean workItemExists(int ID) {
        return engine.getWorkItems().containsKey(ID);
    }

    protected WorkItem findWorkItem(int ID) {
        if (!workItemExists(ID)) {
            throw new IllegalArgumentException(String.format("Workitem with ID: %d was not found!", ID));
        }
        return engine.getWorkItems().get(ID);
    }

    protected boolean boardExists(String teamName, String boardName) {
        Team team = findTeam(teamName);
        List<Board> boards = team.getBoards();
        return boards.stream().anyMatch((Board board) -> board.getName().equals(boardName));
    }

    protected Board findBoard(String teamName, String boardName) {
        if (!boardExists(teamName,boardName)){
            throw new IllegalArgumentException(String.format("Board %s was not found in team %s!",boardName,teamName));
        }
        List<Board> boards = findTeam(teamName).getBoards();
        return boards.stream().filter((Board board) -> board.getName().equals(boardName)).findFirst().get();
    }
    protected String[] parseOption(String parameters, int limit){
        String[] parts = parameters.split(" ", limit);
        if (parts.length!= limit){
            throw new IllegalArgumentException("Incorrect value for option!");
        }
        return parts;
    }
}

