package com.company.commands.showCommands;

import com.company.commands.CommandBase;
import com.company.core.contracts.Engine;
import com.company.core.factories.FactoryWIM;
import com.company.models.WorkItems.BugImpl;
import com.company.models.WorkItems.FeedbackImpl;
import com.company.models.WorkItems.contractsWorkItem.*;
import com.company.models.common.StatusType;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListCommand extends CommandBase {
    private final FactoryWIM factoryWIM;

    public ListCommand(Engine engine, FactoryWIM factoryWIM) {
        super(engine);
        this.factoryWIM = factoryWIM;
    }

    private List<WorkItem> getWorkItems(Map<String, String> parameters) {


        if (parameters.containsKey("person")) {
            Member member = findMember(parameters.get("person"));
            return member.getWorkItem();
        }

        if (parameters.containsKey("team")) {
            Team team = findTeam(parameters.get("team"));
            List<WorkItem> workItems = new ArrayList<>();
            for (Member member : team.getMembers()) {
                workItems.addAll(member.getWorkItem());
            }
            return workItems;
        }

        if (parameters.containsKey("board")) {
            String[] parts = parseOption(parameters.get("board"), 2);
            Board board = findBoard(parts[0], parts[1]);
            return board.getWorkItem();
        }
        return new ArrayList<>(getEngine().getWorkItems().values());
    }

    private List<WorkItem> filterByType(List<WorkItem> workItems, String type) {
        return workItems.stream()
                .filter((WorkItem workItem) -> workItem.getClass().getSimpleName().replace("Impl", "").equalsIgnoreCase(type))
                .collect(Collectors.toList());
    }

    private List<WorkItem> filterByStatus(List<WorkItem> workItems, String status) {
        StatusType statusType = factoryWIM.getStatus(status);
        return workItems.stream()
                .filter((WorkItem workItem) -> workItem.getStatus() == statusType)
                .collect(Collectors.toList());
    }

    private List<WorkItem> filterByAssignee(List<WorkItem> workItems, String assignee) {
        Member member = findMember(assignee);
        return workItems.stream()
                .filter(workItem -> workItem instanceof Assignable)
                .filter((WorkItem workItem) -> ((Assignable) workItem).getAssignee() == member)
                .collect(Collectors.toList());
    }

    private List<WorkItem> sortBy(List<WorkItem> workItems, String property) {
        switch (property.toLowerCase()) {
            case "title":
                return workItems.stream()
                        .sorted((WorkItem wi1, WorkItem wi2) -> wi1.getTitle().compareTo(wi2.getTitle()))
                        .collect(Collectors.toList());
            case "priority":
                return workItems.stream().filter(workItem -> workItem instanceof Prioritised)
                        .sorted((WorkItem wi1, WorkItem wi2) -> ((Prioritised) wi1).getPriority().compareTo(((Prioritised) wi2).getPriority()))
                        .collect(Collectors.toList());
            case "severity":
                return workItems.stream().filter(workItem -> workItem instanceof Bug)
                        .sorted((WorkItem wi1, WorkItem wi2) -> ((Bug) wi1).getSeverity().compareTo(((Bug) wi2).getSeverity()))
                        .collect(Collectors.toList());
            case "size":
                return workItems.stream().filter(workItem -> workItem instanceof Story)
                        .sorted((WorkItem wi1, WorkItem wi2) -> ((Story) wi1).getSize().compareTo(((Story) wi2).getSize()))
                        .collect(Collectors.toList());
            case "rating":
                return workItems.stream().filter(workItem -> workItem instanceof Feedback)
                        .sorted((WorkItem wi1, WorkItem wi2) -> ((Feedback) wi1).getRating() - (((Feedback) wi2).getRating()))
                        .collect(Collectors.toList());

            default:
                throw new IllegalArgumentException("Unknown sorting type!");
        }
    }

    @Override
    public String execute(Map<String, String> parameters) {
        List<WorkItem> workItems = getWorkItems(parameters);
        if (parameters.containsKey("bugs")) {
            workItems = filterByType(workItems, "bug");
        }
        if (parameters.containsKey("stories")) {
            workItems = filterByType(workItems, "story");
        }
        if (parameters.containsKey("feedbacks")) {
            workItems = filterByType(workItems, "feedback");
        }
        if (parameters.containsKey("status")) {
            workItems = filterByStatus(workItems, parameters.get("status"));
        }
        if (parameters.containsKey("assignee")) {
            workItems = filterByAssignee(workItems, parameters.get("assignee"));
        }
        if (parameters.containsKey("sort")) {
            workItems = sortBy(workItems, parameters.get("sort"));
        }
        if (parameters.containsKey("people")) {
            List<String> infos1 = new ArrayList<>();
            for (Member member : getEngine().getMembers().values()) {
                infos1.add(member.getName());
            }
            return String.join(System.getProperty("line.separator"), infos1);
        }
        if (parameters.containsKey("teams")) {
            List<String> infos2 = new ArrayList<>();
            for (Team team : getEngine().getTeams().values()) {
                infos2.add(team.getName());
            }
            return String.join(System.getProperty("line.separator"), infos2);
        }

        List<String> infos = new ArrayList<>();
        for (WorkItem workItem : workItems) {
            infos.add(workItem.getTitle());
        }
        return String.join(System.getProperty("line.separator"), infos);
    }
}
