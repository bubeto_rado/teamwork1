package com.company.commands.showCommands;

import com.company.commands.CommandBase;
import com.company.core.contracts.Engine;
import com.company.models.WorkItems.contractsWorkItem.Assignable;
import com.company.models.WorkItems.contractsWorkItem.WorkItem;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;

import java.util.Map;

public class AssignCommand extends CommandBase {
    public AssignCommand(Engine engine) {
        super(engine);
    }

    @Override
    public String execute(Map<String, String> parameters) {
        if(!parameters.containsKey("person") || !parameters.containsKey("workitem")) {
            throw new IllegalArgumentException("You must provide person and work item ID!");
        }
        Member member = findMember(parameters.get("person"));
        int ID = 0;
        try {
            ID = Integer.parseInt(parameters.get("workitem"));
        } catch (Exception e) {
            throw new IllegalArgumentException("ID must be integer!");
        }
        WorkItem workItem = findWorkItem(ID);
        if(workItem.getClass().getSimpleName().equals("FeedbackImpl")) {
            throw new IllegalArgumentException("You can't assign feedback!");
        }
        if(!checkAssign(member, workItem)) {
            throw new IllegalArgumentException(String.format("You can't assign %s to work item with ID:%d!", member.getName(), workItem.getID()));
        }
        member.addWorkItem(workItem);
        Assignable bugStory = (Assignable) workItem;
        bugStory.setAssignee(member);

        return String.format("Work item with ID: %d successfully assigned to %s!", bugStory.getID(), member.getName());
    }

    private boolean checkAssign(Member member, WorkItem workItem) {
        for(Team team : getEngine().getTeams().values()) {
            if(!team.getMembers().contains(member)) {
                continue;
            }
            for(Board board : team.getBoards()) {
                if(board.getWorkItem().contains(workItem)) {
                    return true;
                }
            }
        }
        return false;
    }
}
