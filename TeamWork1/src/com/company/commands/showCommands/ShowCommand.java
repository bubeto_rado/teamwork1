package com.company.commands.showCommands;

import com.company.commands.CommandBase;
import com.company.core.contracts.Engine;
import com.company.models.WorkItems.contractsWorkItem.WorkItem;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ShowCommand extends CommandBase {
    public ShowCommand(Engine engine) {
        super(engine);
    }

    private String showPerson(String name) {
        Member member = findMember(name);
        return member.toString();
    }

    private String showTeam(String name) {
        Team team = findTeam(name);
        return team.toString();
    }

    private String showBoard(String teamName, String boardName) {
        Board board = findBoard(teamName, boardName);
        return board.toString();
    }

    private String showWorkItem(int ID) {
        WorkItem workItem = findWorkItem(ID);
        return workItem.toString();
    }

    private String showAllPeople (){

        return getEngine().getMembers().toString();

    }

    @Override
    public String execute(Map<String, String> parameters) {
        if (parameters.size() != 1) {
            throw new IllegalArgumentException("Incorrect input data!");
        }
        if (parameters.containsKey("person")) {
            return showPerson(parameters.get("person"));
        }
        if (parameters.containsKey("team")) {
            return showTeam(parameters.get("team"));
        }
        if (parameters.containsKey("board")) {
            String[] parts = parseOption(parameters.get("board"), 2);
            return showBoard(parts[0], parts[1]);
        }
        if (parameters.containsKey("people")){
            return showAllPeople();
        }
        if (parameters.containsKey("boards")){
            ArrayList <String > infos = new ArrayList<>();
            Team team = findTeam(parameters.get("boards"));
            for (Board board : team.getBoards()) {
                infos.add(board.getName());
            }
            return String.join(System.getProperty("line.separator"),infos);
        }
        if (parameters.containsKey("members")){
            ArrayList<String> infos = new ArrayList<>();
            Team team = findTeam(parameters.get("members"));
            for (Member member : team.getMembers()){
                infos.add(member.getName());
            }
            return String.join(System.getProperty("line.separator"),infos);

        }
        if (parameters.containsKey("workitem")){
            int ID = 0;
            try {
                ID = Integer.parseInt(parameters.get("workitem"));
            }catch (Exception e){
                throw new IllegalArgumentException("ID must be integer!");
            }
            return showWorkItem(ID);
        }
        throw new IllegalArgumentException("Invalid option for show command!");
    }
}
