package com.company.commands.showCommands;

import com.company.commands.CommandBase;
import com.company.core.contracts.Engine;
import com.company.models.WorkItems.contractsWorkItem.Assignable;
import com.company.models.WorkItems.contractsWorkItem.WorkItem;
import com.company.models.contracts.Member;

import java.util.Map;

public class UnassignCommand extends CommandBase {
    public UnassignCommand(Engine engine) {
        super(engine);
    }

    @Override
    public String execute(Map<String, String> parameters) {
        if(!parameters.containsKey("workitem")) {
            throw new IllegalArgumentException("You must provide work item's ID!");
        }
        int ID = 0;
        try {
            ID = Integer.parseInt(parameters.get("workitem"));
        } catch (Exception e) {
            throw new IllegalArgumentException("ID must be integer!");
        }
        WorkItem workItem = findWorkItem(ID);
        if(workItem.getClass().getSimpleName().equals("FeedbackImpl")) {
            throw new IllegalArgumentException("You can't unassign feedback!");
        }
        Assignable assignable = (Assignable) workItem;
        Member member = assignable.getAssignee();
        if(member == null) {
            throw new IllegalArgumentException(String.format("Work item with ID: %d is not assigned to anybody!", workItem.getID()));
        }
        member.removeWorkItem(assignable);
        assignable.setAssignee(null);

        return String.format("Work item with ID:%d successfully unassigned from %s", assignable.getID(), member.getName());
    }
}
