package com.company;

import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.factories.FactoryWIM;
import com.company.core.factories.FactoryWIMImpl;

public class Main {

    public static void main(String[] args) {
        FactoryWIM factoryWIM = new FactoryWIMImpl();
        Engine engine = new EngineImpl(factoryWIM);
        engine.start();
    }
}
