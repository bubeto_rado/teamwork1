package com.company.models;

import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;

import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {
    private static final String NO_SUCH_ITEM_MESSAGE = "There is no such item in the list!";
    private static final String MEMBER_ADDED = "A new member '%s' was added to team '%s'!";
    private static final String BOARD_ADDED = "A new board '%s' was added to team '%s'!";
    private static final String BOARDS_SAME_NAME_MESSAGE = "Two boards with same names can not exist in this universe!";
    private static final String THERE_IS_NO_SUCH_BOARD = "There is no such board!";

    private String name;
    private List<Member> members = new ArrayList<>();
    private List<Board> boards = new ArrayList<>();

    public TeamImpl(String name) {
        setName(name);
    }

    private void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(members);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public void addMember(Member member) {
        members.add(member);
        member.getActivityHistory().add(String.format(MEMBER_ADDED, member, getName()));
    }

    @Override
    public void addBoard(Board board) {
        boards.add(board);
        board.getActivityHistory().add(String.format(BOARD_ADDED, board, getName()));
    }

    @Override
    public void removeMember(Member member) {
        checkIfListContainsObject(member, members);
        members.remove(member);
    }

    @Override
    public void addBoard(String name) {
        for (Board board : boards) {
            if (board.getName().equals(name))
                throw new IllegalArgumentException(BOARDS_SAME_NAME_MESSAGE);
        }
        boards.add(new BoardImpl(name));
    }

    @Override
    public void removeBoard(Board board) {
        checkIfListContainsObject(board, boards);
        boards.remove(board);
    }

    @Override
    public Board getBoard(String name) {
        for (Board board : boards) {
            if (board.getName().equals(name)) {
                return board;
            }
        }
        throw new IllegalArgumentException(THERE_IS_NO_SUCH_BOARD);
    }

    @Override
    public String toString() {
        return String.format("Team name: %s\n" +
                        "Members: %s\n" +
                        "Boards: \n%s\n", getName(),
                members.isEmpty() ? "No members in the team" : getMembers(),
                boards.isEmpty() ? "No boards in the team" : getBoards())
                .replace("[","")
                .replace("]","");
    }

    private void checkIfListContainsObject(Object object, List listOfObjects) {
        if (listOfObjects.contains(object)) {
            throw new IllegalArgumentException(NO_SUCH_ITEM_MESSAGE);
        }
    }
}
