package com.company.models.contracts;

import java.util.List;

public interface Team {
    String getName();

    List<Member> getMembers();

    List<Board> getBoards();

    void addMember(Member member);

    void addBoard(Board board);

    void addBoard(String name);

    void removeMember(Member member);

    void removeBoard(Board board);

    Board getBoard(String name);


}
