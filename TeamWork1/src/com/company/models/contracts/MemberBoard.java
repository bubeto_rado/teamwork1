package com.company.models.contracts;

import com.company.models.WorkItems.contractsWorkItem.WorkItem;
import java.util.List;

public interface MemberBoard {
    String getName();

    List<WorkItem> getWorkItem();

    List<String> getActivityHistory();

    void addWorkItem(WorkItem workItem);

    void removeWorkItem(WorkItem workItem);
}
