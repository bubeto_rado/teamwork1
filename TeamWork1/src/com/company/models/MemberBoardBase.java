package com.company.models;

import com.company.models.WorkItems.contractsWorkItem.WorkItem;
import com.company.models.contracts.MemberBoard;
import java.util.ArrayList;
import java.util.List;

public class MemberBoardBase implements MemberBoard {
    private static final int MIN_NAME_LENGTH = 5;
    private static final int MAX_NAME_LENGTH = 15;
    private static final String INVALID_NAME_LENGTH_MESSAGE = "Name length should be between 5 and 15 symbols!";
    private static final String WORK_ITEM_ADDED = "New work item was added: %s";
    private static final String WORK_ITEM_REMOVED = "Work item was removed: %s";

    private String name;
    private List<WorkItem> listOfWorkItems = new ArrayList<>();
    private List<String> activityHistory = new ArrayList<>();

    public MemberBoardBase(String name) {
        setName(name);
    }

    private void setName(String name) {
        if (name.length() <= MIN_NAME_LENGTH || name.length() >= MAX_NAME_LENGTH) {
            throw new IllegalArgumentException(INVALID_NAME_LENGTH_MESSAGE);
        }
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItem> getWorkItem() {
        return new ArrayList<WorkItem>(listOfWorkItems);
    }

    @Override
    public List<String> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    @Override
    public void addWorkItem(WorkItem workItem) {
        listOfWorkItems.add(workItem);
        activityHistory.add(String.format(WORK_ITEM_ADDED, workItem.getTitle()));
    }

    @Override
    public void removeWorkItem(WorkItem workItem) {
        containsObject(workItem, listOfWorkItems);
        listOfWorkItems.remove(workItem);
        activityHistory.add(String.format(WORK_ITEM_REMOVED, workItem.getTitle()));
    }

    @Override
    public String toString() {
        return String.format("%s\n" +
                        "Name: %s\n" +
                        "List of work items: %s\n" +
                        "Activity history: %s\n",
                this.getClass().getSimpleName().replace("Impl", ""), getName(),
                listOfWorkItems.isEmpty() ? "No work items" : getWorkItem(),
                activityHistory.isEmpty() ? "No activity history" : getActivityHistory());
    }

    private void containsObject(Object object, List list) {
        if (!list.contains(object)) {
            throw new IllegalArgumentException();
        }
    }
}
