package com.company.models.WorkItems;

import com.company.models.common.StatusType;
import com.company.models.WorkItems.contractsWorkItem.Feedback;

public class FeedbackImpl extends WorkItemBase implements Feedback {
    private static final String NOT_NEGATIVE_RATING_MESSAGE = "Rating cannot be a negative number!";
    private static final String CHANGED_RATING = "Rating was changed to: %s!";

    private int rating;

    public FeedbackImpl(String title, String description, int rating) {
        super(title, description, StatusType.NEW);
        setRating(rating);
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public void changeRating(int newRating) {
        setRating(newRating);
        getHistory().add(String.format(CHANGED_RATING, newRating));
    }

    @Override
    public void changeStatus(StatusType status) {
        setStatus(status);
        getHistory().add(String.format(CHANGED_STATUS, status));
    }

    @Override
    protected void setStatus(StatusType status) {
        if (status.equals(StatusType.NEW) || status.equals(StatusType.UNSCHEDULED) || status.equals(StatusType.SCHEDULED) || status.equals(StatusType.DONE)) {
            this.status = status;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    protected String additionalInfo() {
        return String.format("Rating: %s", getRating());
    }

    private void setRating(int rating) {
        if (rating <= 0) {
            reduceCounter();
            throw new IllegalArgumentException(NOT_NEGATIVE_RATING_MESSAGE);
        }
        this.rating = rating;
    }
}
