package com.company.models.WorkItems;

import com.company.models.common.StatusType;
import com.company.models.WorkItems.contractsWorkItem.WorkItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class WorkItemBase implements WorkItem {
    private static final int MIN_NAME_LENGTH = 10;
    private static final int MAX_NAME_LENGTH = 50;
    private static final String INVALID_NAME_LENGTH_MESSAGE = "Name length should be between 10 and 50 symbols!";
    private static final int MIN_DESCRIPTION_LENGTH = 10;
    private static final int MAX_DESCRIPTION_LENGTH = 500;
    private static final String INVALID_DESCRIPTION_LENGTH_MESSAGE = "Description length should be between 10 and 50 symbols!";
    private static final String NEW_COMMENT_MESSAGE = "A new comment was added by %s: %s!";
    protected static final String CHANGED_STATUS = "Status was changed to: %s";

    private String title;
    private String description;
    protected StatusType status;
    private HashMap<String, String> comments = new HashMap<>();
    private List<String> history = new ArrayList<>();

    private int ID;
    private static int counter = 0;

    public WorkItemBase(String title, String description, StatusType status) {
        setTitle(title);
        setDescription(description);
        setStatus(status);
        setCounter();
        setID();
    }

    @Override
    public int getCounter() {
        return counter;
    }

    @Override
    public int getID() {
        return ID;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public StatusType getStatus() {
        return status;
    }

    @Override
    public HashMap<String, String> getComments() {
        return new HashMap<>(comments);
    }

    @Override
    public List<String> getHistory() {
        return new ArrayList<>(history);
    }

    @Override
    public void addComment(String author, String comment) {
        comments.put(author, comment);
        getHistory().add(String.format(NEW_COMMENT_MESSAGE, author, comment));
    }

    @Override
    public String toString() {
        return String.format("Type: %s\n" +
                        "Title: %s\n" +
                        "Description: %s\n" +
                        "Status: %s\n" +
                        "Comments: %s\n" +
                        "History: %s\n" +
                        "%s\n",
                this.getClass().getSimpleName().replace("Impl", ""),
                getTitle(), getDescription(),
                getStatus(),
                comments.isEmpty()?"No comments yet":getComments(),
                history.isEmpty()?"No history yet":getHistory(),
                additionalInfo());
    }

    protected abstract void setStatus(StatusType status);

    protected abstract String additionalInfo();

    protected void reduceCounter() {
        counter--;
    }

    private void setCounter() {
        counter++;
    }

    private void setID() {
        this.ID = getCounter();
    }

    private void setTitle(String title) {
        isValidLength(title, MIN_NAME_LENGTH, MAX_NAME_LENGTH, INVALID_NAME_LENGTH_MESSAGE);
        this.title = title;
    }

    private void setDescription(String description) {
        isValidLength(description, MIN_DESCRIPTION_LENGTH, MAX_DESCRIPTION_LENGTH, INVALID_DESCRIPTION_LENGTH_MESSAGE);
        this.description = description;
    }

    private void isValidLength(String text, int minLength, int maxLength, String exceptionMessage) {
        if (text.length() <= minLength || text.length() >= maxLength) {
            throw new IllegalArgumentException(exceptionMessage);
        }
    }
}
