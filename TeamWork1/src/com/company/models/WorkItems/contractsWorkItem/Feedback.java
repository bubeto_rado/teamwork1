package com.company.models.WorkItems.contractsWorkItem;

public interface Feedback extends WorkItem {
    int getRating();

    void changeRating(int newRating);
}
