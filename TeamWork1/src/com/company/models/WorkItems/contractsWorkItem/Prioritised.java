package com.company.models.WorkItems.contractsWorkItem;

import com.company.models.common.PriorityType;

public interface Prioritised extends WorkItem {
    PriorityType getPriority();

    void changePriority(PriorityType priority);
}
