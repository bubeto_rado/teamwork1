package com.company.models.WorkItems.contractsWorkItem;

import com.company.models.common.SizeType;

public interface Story extends Assignable,Prioritised {
    SizeType getSize();

    void changeSize(SizeType size);
}
