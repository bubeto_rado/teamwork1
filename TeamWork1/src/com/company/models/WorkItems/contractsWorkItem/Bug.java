package com.company.models.WorkItems.contractsWorkItem;

import com.company.models.common.SeverityType;
import java.util.List;

public interface Bug extends Assignable,Prioritised {
    List<String> getStepsToReproduce();

    SeverityType getSeverity();

    void changeSeverity(SeverityType severity);
}
