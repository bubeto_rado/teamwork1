package com.company.models.WorkItems.contractsWorkItem;

import com.company.models.contracts.Member;

public interface Assignable extends WorkItem {
    Member getAssignee();

    void setAssignee(Member member);
}
