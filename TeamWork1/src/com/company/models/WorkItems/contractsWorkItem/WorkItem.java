package com.company.models.WorkItems.contractsWorkItem;

import com.company.models.common.StatusType;
import java.util.HashMap;
import java.util.List;

public interface WorkItem {
    String getTitle();

    String getDescription();

    StatusType getStatus();

    int getID();

    HashMap<String, String> getComments();

    List<String> getHistory();

    int getCounter();

    void changeStatus(StatusType status);

    void addComment(String author, String comment);
}
