package com.company.models.WorkItems;

import com.company.models.WorkItems.contractsWorkItem.Assignable;
import com.company.models.WorkItems.contractsWorkItem.Prioritised;
import com.company.models.common.PriorityType;
import com.company.models.common.StatusType;
import com.company.models.contracts.Member;

public abstract class MemberWorkItem extends WorkItemBase implements Assignable, Prioritised {
    private static final String CHANGED_PRIORITY = "Priority was changed to: %s!";

    private PriorityType priority;
    private Member assignee;

    public MemberWorkItem(String title, String description, StatusType status, PriorityType priority) {
        super(title, description, status);
        setPriority(priority);
    }

    public void setAssignee(Member assignee) {
        this.assignee = assignee;
    }

    @Override
    public PriorityType getPriority() {
        return priority;
    }

    @Override
    public Member getAssignee() {
        return assignee;
    }

    @Override
    public void changePriority(PriorityType priority) {
        setPriority(priority);
        getHistory().add(String.format(CHANGED_PRIORITY, priority));
    }

    @Override
    protected String additionalInfo() {
        return String.format("Priority: %s\n" +
                "Assignee: %s\n", getPriority(), assignee != null ? assignee.getName() : "unassigned");
    }

    private void setPriority(PriorityType priority) {
        if (priority.equals(PriorityType.HIGH) || priority.equals(PriorityType.MEDIUM) || priority.equals(PriorityType.LOW)) {
            this.priority = priority;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
