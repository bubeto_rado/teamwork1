package com.company.models.WorkItems;

import com.company.models.common.PriorityType;
import com.company.models.common.SizeType;
import com.company.models.common.StatusType;
import com.company.models.WorkItems.contractsWorkItem.Story;

public class StoryImpl extends MemberWorkItem implements Story {
    private static final String CHANGED_SIZE = "Size was changed to: %s";

    private SizeType size;

     public StoryImpl(String title, String description, PriorityType priority) {
        super(title, description, StatusType.NOT_DONE, priority);
        setStatus(StatusType.NOT_DONE);
        setSize(SizeType.SMALL);
    }

    @Override
    public SizeType getSize() {
        return size;
    }

    @Override
    protected void setStatus(StatusType status) {
        if (status.equals(StatusType.NOT_DONE) || status.equals(StatusType.IN_PROGRESS) || status.equals(StatusType.DONE)) {
            this.status = status;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void changeStatus(StatusType status) {
        setStatus(status);
        getHistory().add(String.format(CHANGED_STATUS, status));
    }

    @Override
    public void changeSize(SizeType size) {
        setSize(size);
        getHistory().add(String.format(CHANGED_SIZE, size));
    }

    @Override
    protected String additionalInfo() {
        return String.format("Size: %s", getSize());
    }

    private void setSize(SizeType size) {
        if (!(size.equals(SizeType.LARGE) || size.equals(SizeType.SMALL) || size.equals(SizeType.MEDIUM))) {
            reduceCounter();
            throw new IllegalArgumentException();
        }
        this.size = size;
    }
}
