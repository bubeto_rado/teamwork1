package com.company.models.WorkItems;

import com.company.models.common.PriorityType;
import com.company.models.common.SeverityType;
import com.company.models.common.StatusType;
import com.company.models.WorkItems.contractsWorkItem.Bug;
import java.util.ArrayList;
import java.util.List;

public class BugImpl extends MemberWorkItem implements Bug {
    private static final String CHANGED_SEVERITY = "Severity was changed to: %s!";

    private List<String> stepsToReproduce = new ArrayList<>();
    private SeverityType severity;

    public BugImpl(String title, String description, PriorityType priority, List<String> stepsToReproduce, SeverityType severity) {
        super(title, description, StatusType.ACTIVE, priority);
        setStepsToReproduce(stepsToReproduce);
        setSeverity(severity);
        setStatus(StatusType.ACTIVE);
    }

    @Override
    public List<String> getStepsToReproduce() {
        return new ArrayList<>(stepsToReproduce);
    }

    @Override
    public SeverityType getSeverity() {
        return severity;
    }

    @Override
    public void changeSeverity(SeverityType severity) {
        setSeverity(severity);
        getHistory().add(String.format(CHANGED_SEVERITY, severity));
    }

    @Override
    public void changeStatus(StatusType status) {
        setStatus(status);
        getHistory().add(String.format(CHANGED_STATUS, status));
    }

    @Override
    protected void setStatus(StatusType status) {
        if (status.equals(StatusType.ACTIVE) || status.equals(StatusType.FIXED)) {
            this.status = status;
        } else {
            throw new IllegalArgumentException("Invalid status type for bug!");
        }
    }

    @Override
    protected String additionalInfo() {
        return super.additionalInfo() + String.format("Steps to reproduce: %s\n" +
                "Severity: %s",
                stepsToReproduce.isEmpty()?"No steps to reproduce yet":getStepsToReproduce(),
                getSeverity());
    }

    private void setStepsToReproduce(List<String> stepsToReproduce) {
        this.stepsToReproduce = new ArrayList<>(stepsToReproduce);
    }

    private void setSeverity(SeverityType severity) {
        if (!(severity.equals(SeverityType.CRITICAL) || severity.equals(SeverityType.MAJOR) || severity.equals(SeverityType.MINOR))) {
            reduceCounter();
            throw new IllegalArgumentException();
        }
        this.severity = severity;
    }
}
