import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.factories.FactoryWIMImpl;
import com.company.core.providers.CommandParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.Map;

public class AddCommandTest {

   private FactoryWIMImpl factory;
    private EngineImpl engine;
    private CommandParser commandParser;

    @Before
    public void beforeTestInitializeFactoryEngineParser() {
        factory = new FactoryWIMImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory, engine);
    }

    @Test
    public void commandAddMemberExistingTeam() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandMember = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN);
        Map<String,String> testParametersMember = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN);
        String resultMember = testCommandMember.execute(testParametersMember);

        Command testCommand = commandParser.parseCommand(Constants.ADD_PERSON_TO_TEAM_MARTIN_AWESOME);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.ADD_PERSON_TO_TEAM_MARTIN_AWESOME);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.MARTIN_ADDED_SUCCESSFULLY_TO_TEAM_AWESOME, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandAddMemberNotExistingTeam() {
        Command testCommand = commandParser.parseCommand(Constants.ADD_PERSON_TO_TEAM_MARTIN_AWESOME);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.ADD_PERSON_TO_TEAM_MARTIN_AWESOME);
        String result = testCommand.execute(testParameters);
        }

    @Test(expected = IllegalArgumentException.class)
    public void commandAddMemberNotExistingMember() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommand = commandParser.parseCommand(Constants.ADD_PERSON_TO_TEAM_MARTIN_AWESOME);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.ADD_PERSON_TO_TEAM_MARTIN_AWESOME);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandAddCommentNoParameters() {
        Command testCommand = commandParser.parseCommand(Constants.ADD_COMMENT);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.ADD_COMMENT);
        String result = testCommand.execute(testParameters);
    }
}
