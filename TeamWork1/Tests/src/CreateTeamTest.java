import com.company.core.factories.FactoryWIMImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Test;

public class CreateTeamTest {
    @Test
    public void returnInstanceOfTypeMember() {
        // Arrange
        FactoryWIMImpl factory = new FactoryWIMImpl();
        // Act
        Team team = factory.createTeam(Constants.SAMPLE_NAME);
        // Assert
        Assert.assertTrue(team instanceof TeamImpl);
    }
}
