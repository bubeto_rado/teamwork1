import com.company.core.factories.FactoryWIMImpl;
import com.company.models.WorkItems.BugImpl;
import com.company.models.WorkItems.WorkItemBase;
import com.company.models.WorkItems.contractsWorkItem.Bug;
import com.company.models.common.PriorityType;
import com.company.models.common.SeverityType;
import com.company.models.common.StatusType;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class CreateBugTest {

    @Test
    public void returnInstanceOfTypeBug() {
        // Arrange
        FactoryWIMImpl factory = new FactoryWIMImpl();
        // Act
        Bug bug = factory.createBug(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, "High",
                new ArrayList<>(), "Major");
        // Assert
        Assert.assertTrue(bug instanceof WorkItemBase);
    }

    @Test
    public void createBug() {
        BugImpl bug = new BugImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, PriorityType.HIGH,
                new ArrayList<>(), SeverityType.MAJOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugTitleIsLongerThanMaxValue() {
        BugImpl bug = new BugImpl(Constants.LONGER_THAN_50_SYMBOLS_TITLE,
                Constants.SAMPLE_DESCRIPTION, PriorityType.MEDIUM, new ArrayList<>(), SeverityType.CRITICAL);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugTitleIsShorterThanMinValue() {
        BugImpl bug = new BugImpl(Constants.SHORTER_THAT_10_SYMBOLS_TITLE, Constants.SAMPLE_DESCRIPTION, PriorityType.HIGH,
                new ArrayList<>(), SeverityType.MINOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugDescriptionIsShorterThanMinValue() {
        BugImpl bug = new BugImpl(Constants.SAMPLE_TITLE, Constants.SHORTER_THAN_10_SYMBOLS_DESCRIPTION, PriorityType.LOW,
                new ArrayList<>(), SeverityType.CRITICAL);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugDescriptionIsLongerThanMaxValue() {

        BugImpl bug = new BugImpl(Constants.SAMPLE_TITLE, Constants.LONGER_THAN_500_SYMBOLS_DESCRIPTION, PriorityType.HIGH,
                new ArrayList<>(), SeverityType.MAJOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStatusIsNotRight() {
        BugImpl bug = new BugImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, PriorityType.MEDIUM,
                new ArrayList<>(), SeverityType.MAJOR);
        bug.changeStatus(StatusType.NOT_DONE);
    }

    @Test
    public void changeStatus() {
        BugImpl bug = new BugImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, PriorityType.MEDIUM,
                new ArrayList<>(), SeverityType.MINOR);
        bug.changeStatus(StatusType.ACTIVE);
    }

    @Test
    public void changeSeverity() {
        BugImpl bug = new BugImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, PriorityType.MEDIUM,
                new ArrayList<>(), SeverityType.MINOR);
        bug.changeSeverity(SeverityType.MAJOR);
    }

    @Test
    public void changePriority() {
        BugImpl bug = new BugImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, PriorityType.MEDIUM,
                new ArrayList<>(), SeverityType.MINOR);
        bug.changePriority(PriorityType.MEDIUM);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenPriorityIsNotRight() {
        FactoryWIMImpl factory = new FactoryWIMImpl();
        Bug bug = factory.createBug(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, "H",
                new ArrayList<>(), "Minor");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenSeverityIsNotRight() {
        FactoryWIMImpl factory = new FactoryWIMImpl();
        Bug bug = factory.createBug(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, "Low",
                new ArrayList<>(), "M");
    }
}
