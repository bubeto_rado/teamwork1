import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.factories.FactoryWIMImpl;
import com.company.core.providers.CommandParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

public class CreateCommandTest {
    private FactoryWIMImpl factory;
    private EngineImpl engine;
    private CommandParser commandParser;

    @Before
    public void beforeTestInitializeFactoryEngineParser() {
        factory = new FactoryWIMImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory, engine);
    }

    @Test
    public void commandCreateTeam() {
        Command testCommand = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.TEAM_WITH_NAME_AWESOME_WAS_CREATED_SUCCESSFULLY, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreateTeamNoName() {
        Command testCommand = commandParser.parseCommand(Constants.CREATE_TEAM_NO_NAME);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_TEAM_NO_NAME);
        String result = testCommand.execute(testParameters);
        }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreateTeamSameName() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String result = testCommand.execute(testParameters);
    }

    @Test
    public void commandCreatePerson() {
        Command testCommand = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.PERSON_WITH_NAME_MARTIN_WAS_CREATED_SUCCESSFULLY, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreatePersonSameName() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN);
        String result = testCommand.execute(testParameters);
    }

    @Test
    public void commandCreateBoard() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.BOARD_WITH_NAME_MY_BOARD_WAS_CREATED_SUCCESSFULLY_IN_TEAM_AWESOME, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreateBoardSameName() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommandSame = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersSame = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultSame = testCommandSame.execute(testParametersSame);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.BOARD_WITH_NAME_MY_BOARD_ALREADY_EXISTS_IN_TEAM_AWESOME, result);
    }

    @Test (expected = IllegalArgumentException.class)
    public void commandCreateBoardNotExistingTeam() {
        Command testCommand = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String result = testCommand.execute(testParameters);
       }

    @Test
    public void commandCreateBug() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommandSecond = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersSecond = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultSecond = testCommandSecond.execute(testParametersSecond);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_BUG);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.BUG_WITH_TITLE_CRASH_AT_START_WAS_SUCCESSFULLY_CREATED, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreateBugNotExistingTeam() {
        Command testCommandSecond = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersSecond = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultSecond = testCommandSecond.execute(testParametersSecond);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_BUG);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.BUG_WITH_TITLE_CRASH_AT_START_WAS_SUCCESSFULLY_CREATED, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreateBugNotExistingBoard() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_BUG);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.BUG_WITH_TITLE_CRASH_AT_START_WAS_SUCCESSFULLY_CREATED, result);
    }

    @Test
    public void commandCreateStory() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommandSecond = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersSecond = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultSecond = testCommandSecond.execute(testParametersSecond);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_STORY);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_STORY);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.STORY_WITH_TITLE_MY_STORY_IN_TELERIK_WAS_SUCCESSFULLY_CREATED, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreateStoryNotExistingTeam() {
       Command testCommandSecond = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersSecond = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultSecond = testCommandSecond.execute(testParametersSecond);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_STORY);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_STORY);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.STORY_WITH_TITLE_MY_STORY_IN_TELERIK_WAS_SUCCESSFULLY_CREATED, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreateStoryNotExistingBoard() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_STORY);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_STORY);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.STORY_WITH_TITLE_MY_STORY_IN_TELERIK_WAS_SUCCESSFULLY_CREATED, result);
    }

    @Test
    public void commandCreateFeedback() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommandSecond = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersSecond = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultSecond = testCommandSecond.execute(testParametersSecond);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_FEEDBACK);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_FEEDBACK);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.FEEDBACK_WITH_TITLE_SAMPLE_FEEDBACK_WAS_SUCCESSFULLY_CREATED, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreateFeedbackNotExistingTeam() {
        Command testCommandSecond = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersSecond = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultSecond = testCommandSecond.execute(testParametersSecond);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_FEEDBACK);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_FEEDBACK);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.FEEDBACK_WITH_TITLE_SAMPLE_FEEDBACK_WAS_SUCCESSFULLY_CREATED, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreateFeedbackNotExistingBoard() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_FEEDBACK);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_FEEDBACK);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.FEEDBACK_WITH_TITLE_SAMPLE_FEEDBACK_WAS_SUCCESSFULLY_CREATED, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreateFeedbackInvalidRating() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommandSecond = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersSecond = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultSecond = testCommandSecond.execute(testParametersSecond);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_FEEDBACK_INVALID_RATING);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_FEEDBACK_INVALID_RATING);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreateNonExistingWorkItem() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommandSecond = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersSecond = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultSecond = testCommandSecond.execute(testParametersSecond);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_NON_EXISTING_WORK_ITEM);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_NON_EXISTING_WORK_ITEM);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandCreateBugNoDescription() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommandSame = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersSame = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultSame = testCommandSame.execute(testParametersSame);

        Command testCommand = commandParser.parseCommand(Constants.CREATE_BUG_NO_DESCRIPTION);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CREATE_BUG_NO_DESCRIPTION);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandNotExisting() {
        Command testCommand = commandParser.parseCommand(Constants.NOT_EXISTING_COMMAND);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.NOT_EXISTING_COMMAND);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.TEAM_WITH_NAME_AWESOME_WAS_CREATED_SUCCESSFULLY, result);
    }
}
