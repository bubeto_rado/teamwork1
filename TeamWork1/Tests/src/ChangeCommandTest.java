import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.factories.FactoryWIMImpl;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ConsoleWriter;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

public class ChangeCommandTest {
    private FactoryWIMImpl factory;
    private EngineImpl engine;
    private CommandParser commandParser;

    @Before
    public void beforeTestInitializeFactoryEngineParser() {
        factory = new FactoryWIMImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory, engine);
    }

    @Test (expected = IllegalArgumentException.class)
    public void commandChangeBugNoParameters() {
        Command testCommand = commandParser.parseCommand(Constants.CHANGE_WORK_ITEM_NO_PARAMETERS);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CHANGE_WORK_ITEM_NO_PARAMETERS);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandChangeBugInvalidID() {
        Command testCommand = commandParser.parseCommand(Constants.CHANGE_WORK_ITEM_INVALID_ID);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CHANGE_WORK_ITEM_INVALID_ID);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandChangeBugInvalidStatusType() {
        Command testCommandFirst = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersFirst = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultFirst = testCommandFirst.execute(testParametersFirst);

        Command testCommandSecond = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersSecond = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultSecond = testCommandSecond.execute(testParametersSecond);

        Command testCommandStory = commandParser.parseCommand(Constants.CREATE_STORY);
        Map<String, String> testParametersStory = commandParser.parseParameters(Constants.CREATE_STORY);
        String resultStory = testCommandStory.execute(testParametersStory);

        Command testCommand = commandParser.parseCommand(Constants.CHANGE_WORK_ITEM_INVALID_STATUS_TYPE);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.CHANGE_WORK_ITEM_INVALID_STATUS_TYPE);
        String result = testCommand.execute(testParameters);
    }
}
