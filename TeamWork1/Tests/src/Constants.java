import java.util.Collections;

public class Constants {
    static final String CREATE_TEAM_AWESOME = "Create -team Awesome";
    static final String CREATE_TEAM_AWESOME2 = "Create -team Awesome2";
    static final String CREATE_TEAM_NO_NAME = "Create -team";
    static final String CREATE_BOARD_MY_BOARD = "Create -board Awesome MyBoard";
    static final String CREATE_BOARD_MY_BOARD2 = "Create -board Awesome2 MyBoard2";
    static final String CREATE_PERSON_MARTIN = "Create -person Martin";
    static final String CREATE_PERSON_MARTIN2 = "Create -person Martin2";
    static final String CREATE_BUG = "Create -bug " +
            "-title Crash at start -board Awesome MyBoard -description Long long description -priority high -severity critical " +
            "-steps start the program # then program runs # then skies opens # and thunder destroyed my pc";
    static final String CREATE_BUG2 = "Create -bug " +
     "-title Crash at start Second -board Awesome MyBoard -description Long long description -priority high -severity major " +
     "-steps start the program # then program runs # then skies opens # and thunder destroyed my pc";
    static final String CREATE_BUG3 = "Create -bug " +
     "-title Crash at end -board Awesome MyBoard -description Long long description -priority high -severity critical " +
     "-steps start the program # then program runs # then skies opens # and thunder destroyed my pc  ";
    static final String CREATE_BUG_NO_DESCRIPTION = "Create -bug " +
     "-bugName Crash at start -board Awesome MyBoard";
    static final String CREATE_STORY = "Create -story " +
     "-title My story in Telerik -board Awesome MyBoard -description A long, long story about Java! -priority high";
    static final String CREATE_STORY2 = "Create -story " +
     "-title My story Second in Telerik -board Awesome MyBoard -description A long, long story about Java! -priority high";
    static final String CREATE_FEEDBACK = "Create -feedback " +
     "-title SampleFeedback -board Awesome MyBoard -description Some random description about the feedback -rating 18";
    static final String CREATE_FEEDBACK2 = "Create -feedback " +
     "-title SampleFeedback Second -board Awesome MyBoard -description Some random description about the feedback -rating 18";
    static final String CREATE_FEEDBACK3 = "Create -feedback " +
            "-title SampleFeedback3 -board Awesome MyBoard -description Some random description about the feedback -rating 8";
    static final String CREATE_FEEDBACK4 = "Create -feedback " +
            "-title SampleFeedbac4 -board Awesome MyBoard -description Some random description about the feedback -rating 45";

    static final String CREATE_FEEDBACK_INVALID_RATING = "Create -feedback " +
     "-title SampleFeedback -board Awesome MyBoard -description Some random description about the feedback -rating Hi";
    static final String CREATE_NON_EXISTING_WORK_ITEM = "Create -WorkItem ";
    static final String LIST_ALL = "List";
    static final String LIST_ALL_RESULT = "Crash at start\r\n" +
            "SampleFeedback";
    static final String LIST_ALL_PEOPLE = "List -people";
    static final String LIST_ALL_PEOPLE_RESULT = "Martin2\r\n"+
     "Martin";
    static final String LIST_ALL_FEEDBACKS = "List -feedbacks";
    static final String LIST_ALL_FEEDBACKS_RESULT = "SampleFeedback\r\n"+
     "SampleFeedback Second";
    static final String LIST_ALL_STORIES = "List -stories";
    static final String LIST_ALL_STORIES_RESULT = "My story in Telerik\r\n" +
     "My story Second in Telerik";
    static final String LIST_ALL_BUGS = "List -bugs";
    static final String LIST_ALL_BUGS_RESULT = "Crash at start";
    static final String LIST_SORTED_SEVERITY = "List -sort severity";
    static final String LIST_SORTED_RATING = "List -sort rating";
    static final String LIST_SORTED_RATING_RESULT = "SampleFeedback3\r\n" +
            "SampleFeedback Second\r\n" +
            "SampleFeedbac4";
    static final String LIST_SORTED_BY_TITLE= "List -sort title";
    static final String LIST_SORTED_BY_UNKNOWN_TYPE = "List -sort unknown";
    static final String LIST_SORTED_BY_ASSIGNEE = "List -assignee Martin";
    static final String LIST_SORTED_BY_TITLE_RESULT= "Crash at end\r\n" +
     "Crash at start\r\n" +
     "Crash at start Second\r\n" +
     "My story in Telerik";
    static final String LIST_STATUS_ACTIVE = "List -status Active";
    static final String LIST_ASSIGNEE_MARTIN = "List -assignee Martin";
    static final String ADD_PERSON_TO_TEAM_MARTIN_AWESOME = "Add -person Martin Awesome";
    static final String SHOW_PERSON_MARTIN = "Show -person Martin";
    static final String SHOW_PEOPLE= "Show -people";
    static final String SHOW_PEOPLE_RESULT= "{Martin2=Member\n" +
            "Name: Martin2\n" +
            "List of work items: No work items\n" +
            "Activity history: No activity history\n" +
            ", Martin=Member\n" +
            "Name: Martin\n" +
            "List of work items: No work items\n" +
            "Activity history: No activity history\n" +
            "}";
    static final String SHOW_TEAM_AWESOME = "Show -team Awesome";
    static final String SHOW_NO_PARAMETERS = "Show";
    static final String SHOW_INVALID_OPTION = "Show -books";
    static final String SHOW_WORKITEM = "Show -workitem 99999999";
    static final String SHOW_WORKITEM_INVALID_ID = "Show -workitem Hi";
    static final String SHOW_TEAM_MEMBERS_AWESOME = "Show -members Awesome";
    static final String SHOW_TEAM_MEMBERS_AWESOME_RESULT = "Martin";
    static final String SHOW_TEAM_BOARDS_AWESOME = "Show -boards Awesome2";
    static final String SHOW_TEAM_BOARDS_AWESOME_RESULT = "MyBoard2";
    static final String SHOW_MY_BOARD= "Show -board Awesome MyBoard";
    static final String SHOW_MY_BOARD_NO_PARAMETERS = "Show -board Awesome";
    static final String SHOW_BOARD_NOT_EXISTING_BOARD = "Show -board Awesome NotExisting";
    static final String CHANGE_WORK_ITEM_NO_PARAMETERS = "Change -workitem";
    static final String CHANGE_WORK_ITEM_INVALID_ID = "Change -workitem Hi -severity Major";
    static final String CHANGE_WORK_ITEM_INVALID_STATUS_TYPE = "Change -workitem 1 -status No";
    static final String ASSIGN_NO_PARAMETERS = "Assign";
    static final String UNASSIGN_NO_PARAMETERS = "Unassign";
    static final String ASSIGN_NOT_EXISTING_WORK_ITEM = "Assign -person Martin -workitem 99999999";
    static final String UNASSIGN_NOT_EXISTING_WORK_ITEM = "Unassign -person Martin -workitem 99999999";
    static final String UNASSIGN_NOT_VALID_WORK_ITEM_ID = "Unassign -person Martin -workitem Hi";
    static final String ADD_COMMENT = "Add -comment";
    static final String NOT_EXISTING_COMMAND = "Jump";
    static final String BUG_WITH_TITLE_CRASH_AT_START_WAS_SUCCESSFULLY_CREATED = "Bug with title: Crash at start was successfully created!";
    static final String STORY_WITH_TITLE_MY_STORY_IN_TELERIK_WAS_SUCCESSFULLY_CREATED = "Story with title: My story in Telerik was successfully created!";
    static final String FEEDBACK_WITH_TITLE_SAMPLE_FEEDBACK_WAS_SUCCESSFULLY_CREATED = "Feedback with title: SampleFeedback was successfully created!";
    static final String TEAM_WITH_NAME_AWESOME_WAS_CREATED_SUCCESSFULLY = "Team with name Awesome was created successfully.";
    static final String BOARD_WITH_NAME_MY_BOARD_WAS_CREATED_SUCCESSFULLY_IN_TEAM_AWESOME = "Board with name MyBoard was created successfully in team Awesome.";
    static final String BOARD_WITH_NAME_MY_BOARD_ALREADY_EXISTS_IN_TEAM_AWESOME = "Board with name MyBoard already exists in team Awesome!";
    static final String PERSON_WITH_NAME_MARTIN_WAS_CREATED_SUCCESSFULLY = "Person with name Martin was created successfully.";
    static final String MARTIN_ADDED_SUCCESSFULLY_TO_TEAM_AWESOME = "Martin added successfully to team Awesome!";
    static final String SHOW_BOARD_MY_BOARD_RESULT = "Board\n"+
    "Name: MyBoard\n"+
    "List of work items: No work items\n"+
    "Activity history: No activity history\n";

    static final String LIST_SORTED_RESULT = "Crash at start\r\n"+
    "Crash at start Second";
    static final String LIST_STATUS_RESULT = "Crash at start";
    static final String SHOW_PERSON_RESULT = "Member\n" +
            "Name: Martin\n" +
            "List of work items: No work items\n" +
            "Activity history: No activity history\n";
    static final String SHOW_TEAM_AWESOME_RESULT = "Team name: Awesome\n" +
     "Members: No members in the team\n" +
     "Boards: \n"+
     "No boards in the team\n";
    static final String SAMPLE_NAME = "SampleName";
    static final String LONGER_THAN_15_SYMBOLS_NAME = "LongerThan15symbolsName";
    static final String SHORTER_THAN_5_SYMBOLS_NAME = "Name";
    static final String SAMPLE_TITLE = "SampleTitle";
    static final String SHORTER_THAN_10_SYMBOLS_DESCRIPTION = "D";
    static final String LONGER_THAN_500_SYMBOLS_DESCRIPTION = String.join("", Collections.nCopies(501, "a"));
    static final String LONGER_THAN_50_SYMBOLS_TITLE = String.join("", Collections.nCopies(51, "a"));
    static final String SAMPLE_DESCRIPTION = "Some description about the work item";
    static final String SHORTER_THAT_10_SYMBOLS_TITLE = "Title";
}
