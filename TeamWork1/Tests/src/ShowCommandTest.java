import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.factories.FactoryWIMImpl;
import com.company.core.providers.CommandParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class ShowCommandTest {
    private FactoryWIMImpl factory;
    private EngineImpl engine;
    private CommandParser commandParser;

    @Before
    public void beforeTestInitializeFactoryEngineParser() {
        factory = new FactoryWIMImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory, engine);
    }


    @Test
    public void commandShowPerson() {
        Command testCommandMember = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN);
        Map<String, String> testParametersMember = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN);
        String resultMember = testCommandMember.execute(testParametersMember);

        Command testCommand = commandParser.parseCommand(Constants.SHOW_PERSON_MARTIN);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_PERSON_MARTIN);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.SHOW_PERSON_RESULT, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandShowPersonNotExistingPerson() {
        Command testCommand = commandParser.parseCommand(Constants.SHOW_PERSON_MARTIN);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_PERSON_MARTIN);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.SHOW_PERSON_RESULT, result);
    }


    @Test
    public void commandShowBoard() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommand = commandParser.parseCommand(Constants.SHOW_MY_BOARD);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_MY_BOARD);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.SHOW_BOARD_MY_BOARD_RESULT, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandShowBoardNoParameters() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommand = commandParser.parseCommand(Constants.SHOW_MY_BOARD_NO_PARAMETERS);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_MY_BOARD_NO_PARAMETERS);
        String result = testCommand.execute(testParameters);
    }


    @Test(expected = IllegalArgumentException.class)
    public void commandShowBoardNotExisting() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommand = commandParser.parseCommand(Constants.SHOW_BOARD_NOT_EXISTING_BOARD);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_BOARD_NOT_EXISTING_BOARD);
        String result = testCommand.execute(testParameters);
    }

    @Test
    public void commandShowTeam() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommand = commandParser.parseCommand(Constants.SHOW_TEAM_AWESOME);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_TEAM_AWESOME);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.SHOW_TEAM_AWESOME_RESULT, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandShowTeamNotExisting() {
        Command testCommand = commandParser.parseCommand(Constants.SHOW_TEAM_AWESOME);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_TEAM_AWESOME);
        String result = testCommand.execute(testParameters);
    }

    @Test
    public void commandShowAllMembers() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandMember = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN);
        Map<String, String> testParametersMember = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN);
        String resultMember = testCommandMember.execute(testParametersMember);

        Command testCommandMember2 = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN2);
        Map<String, String> testParametersMember2 = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN2);
        String resultMember2 = testCommandMember2.execute(testParametersMember2);

        Command testCommandAdd = commandParser.parseCommand(Constants.ADD_PERSON_TO_TEAM_MARTIN_AWESOME);
        Map<String, String> testParametersAdd = commandParser.parseParameters(Constants.ADD_PERSON_TO_TEAM_MARTIN_AWESOME);
        String resultAdd = testCommandAdd.execute(testParametersAdd);

        Command testCommand = commandParser.parseCommand(Constants.SHOW_TEAM_MEMBERS_AWESOME);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_TEAM_MEMBERS_AWESOME);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.SHOW_TEAM_MEMBERS_AWESOME_RESULT, result);
    }

    @Test
    public void commandShowAllTeamBoards() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandTeam2 = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME2);
        Map<String, String> testParametersTeam2 = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME2);
        String resultTeam2 = testCommandTeam2.execute(testParametersTeam2);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandBoard2 = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD2);
        Map<String, String> testParametersBoard2 = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD2);
        String resultBoard2 = testCommandBoard2.execute(testParametersBoard2);

        Command testCommand = commandParser.parseCommand(Constants.SHOW_TEAM_BOARDS_AWESOME);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_TEAM_BOARDS_AWESOME);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.SHOW_TEAM_BOARDS_AWESOME_RESULT, result);
    }

    @Test
    public void commandShowPeople() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandMember = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN);
        Map<String, String> testParametersMember = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN);
        String resultMember = testCommandMember.execute(testParametersMember);

        Command testCommandMember2 = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN2);
        Map<String, String> testParametersMember2 = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN2);
        String resultMember2 = testCommandMember2.execute(testParametersMember2);

        Command testCommandAdd = commandParser.parseCommand(Constants.ADD_PERSON_TO_TEAM_MARTIN_AWESOME);
        Map<String, String> testParametersAdd = commandParser.parseParameters(Constants.ADD_PERSON_TO_TEAM_MARTIN_AWESOME);
        String resultAdd = testCommandAdd.execute(testParametersAdd);

        Command testCommand = commandParser.parseCommand(Constants.SHOW_PEOPLE);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_PEOPLE);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.SHOW_PEOPLE_RESULT, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandShowWorkItem() {
        Command testCommand = commandParser.parseCommand(Constants.SHOW_WORKITEM);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_WORKITEM);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandShowWorkItemInvalidID() {
        Command testCommand = commandParser.parseCommand(Constants.SHOW_WORKITEM_INVALID_ID);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_WORKITEM_INVALID_ID);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandShowNoParameters() {
        Command testCommand = commandParser.parseCommand(Constants.SHOW_NO_PARAMETERS);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_NO_PARAMETERS);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandShowInvalidOption() {
        Command testCommand = commandParser.parseCommand(Constants.SHOW_INVALID_OPTION);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.SHOW_INVALID_OPTION);
        String result = testCommand.execute(testParameters);
    }
}
