import com.company.core.factories.FactoryWIMImpl;
import com.company.models.WorkItems.FeedbackImpl;
import com.company.models.WorkItems.WorkItemBase;
import com.company.models.WorkItems.contractsWorkItem.Feedback;
import com.company.models.common.StatusType;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;

public class CreateFeedbackTest {

    @Test
    public void returnInstanceOfTypeFeedback() {
        // Arrange
        FactoryWIMImpl factory = new FactoryWIMImpl();
        // Act
        Feedback feedback = factory.createFeedback(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, 8);
        // Assert
        Assert.assertTrue(feedback instanceof WorkItemBase);
    }

    @Test
    public void createFeeddback() {
        FeedbackImpl feedback = new FeedbackImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackTitleIsLongerThanMaxValue() {
        FeedbackImpl feedback = new FeedbackImpl(Constants.LONGER_THAN_50_SYMBOLS_TITLE, Constants.SAMPLE_DESCRIPTION, 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackTitleIsShorterThanMinValue() {
        FeedbackImpl feedback = new FeedbackImpl(Constants.SHORTER_THAT_10_SYMBOLS_TITLE, Constants.SAMPLE_DESCRIPTION, 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackRatingIsNegativeNumber() {
        FeedbackImpl feedback = new FeedbackImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, -8);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackDescriptionIsShorterThanMinValue() {
        FeedbackImpl feedback = new FeedbackImpl(Constants.SAMPLE_TITLE, Constants.SHORTER_THAN_10_SYMBOLS_DESCRIPTION, 8);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackDescriptionIsLongerThanMinValue() {
        FeedbackImpl feedback = new FeedbackImpl(Constants.SAMPLE_TITLE, Constants.LONGER_THAN_500_SYMBOLS_DESCRIPTION, 8);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStatusIsNotRight() {
        FeedbackImpl feedback = new FeedbackImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, 8);
        feedback.changeStatus(StatusType.FIXED);
    }

    @Test
    public void changeStatus() {
        FeedbackImpl feedback = new FeedbackImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, 8);
        feedback.changeStatus(StatusType.NEW);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenRatingIsChangedToNegativeNumber() {
        FeedbackImpl feedback = new FeedbackImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, 8);
        feedback.changeRating(-2);
    }
}
