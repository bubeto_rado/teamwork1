import com.company.core.factories.FactoryWIMImpl;
import com.company.models.BoardImpl;
import com.company.models.MemberBoardBase;
import com.company.models.contracts.Board;
import org.junit.Assert;
import org.junit.Test;

public class CreateBoardTest {


    @Test
    public void returnInstanceOfTypeBug() {
        // Arrange
        FactoryWIMImpl factory = new FactoryWIMImpl();
        // Act
        Board board = new BoardImpl(Constants.SAMPLE_NAME);
        // Assert
        Assert.assertTrue(board instanceof MemberBoardBase);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBoardNameIsLongerThanMaxValue() {
        BoardImpl board = new BoardImpl(Constants.LONGER_THAN_15_SYMBOLS_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBoardNameIsShorterThanMinValue() {
        BoardImpl board = new BoardImpl(Constants.SHORTER_THAN_5_SYMBOLS_NAME);
    }

    @Test
    public void createBoard() {
        BoardImpl board = new BoardImpl(Constants.SAMPLE_NAME);
    }
}
