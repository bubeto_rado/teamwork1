import com.company.core.factories.FactoryWIMImpl;
import com.company.models.MemberBoardBase;
import com.company.models.MemberImpl;
import com.company.models.contracts.Member;
import org.junit.Assert;
import org.junit.Test;

public class CreateMemberTest {
    @Test
    public void returnInstanceOfTypeMember() {
        // Arrange
        FactoryWIMImpl factory = new FactoryWIMImpl();
        // Act
        Member member = factory.createMember(Constants.SAMPLE_NAME);
        // Assert
        Assert.assertTrue(member instanceof MemberBoardBase);
    }

    @Test
    public void createMember() {
        MemberImpl member = new MemberImpl(Constants.SAMPLE_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenMemberNameIsLongerThanMaxValue() {
        MemberImpl member = new MemberImpl(Constants.LONGER_THAN_15_SYMBOLS_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenMemberNameIsShorterThanMinValue() {
        MemberImpl member = new MemberImpl(Constants.SHORTER_THAN_5_SYMBOLS_NAME);
    }
}
