import com.company.core.factories.FactoryWIMImpl;
import com.company.models.WorkItems.StoryImpl;
import com.company.models.WorkItems.WorkItemBase;
import com.company.models.WorkItems.contractsWorkItem.Story;
import com.company.models.common.PriorityType;
import com.company.models.common.SizeType;
import com.company.models.common.StatusType;
import org.junit.Assert;
import org.junit.Test;

public class CreateStoryTest {
    @Test
    public void returnInstanceOfTypeStory() {
        // Arrange
        FactoryWIMImpl factory = new FactoryWIMImpl();
        // Act
        Story story = factory.createStory(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, "High");
        // Assert
        Assert.assertTrue(story instanceof WorkItemBase);
    }

    @Test
    public void createStory() {
        StoryImpl story = new StoryImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, PriorityType.MEDIUM);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryTitleIsLongerThanMaxValue() {
        StoryImpl story = new StoryImpl(Constants.LONGER_THAN_50_SYMBOLS_TITLE, Constants.SAMPLE_DESCRIPTION,
                PriorityType.MEDIUM);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryTitleIsShorterThanMinValue() {
        StoryImpl story = new StoryImpl(Constants.SHORTER_THAT_10_SYMBOLS_TITLE, Constants.SAMPLE_DESCRIPTION,
                PriorityType.MEDIUM);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryDescriptionIsShorterThanMinValue() {
        StoryImpl story = new StoryImpl(Constants.SAMPLE_TITLE, Constants.SHORTER_THAN_10_SYMBOLS_DESCRIPTION,
                PriorityType.LOW);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugDescriptionIsLongerThanMaxValue() {
        StoryImpl story = new StoryImpl(Constants.SAMPLE_TITLE, Constants.LONGER_THAN_500_SYMBOLS_DESCRIPTION,
                PriorityType.HIGH);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStatusIsNotRight() {
        StoryImpl story = new StoryImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, PriorityType.MEDIUM);
        story.changeStatus(StatusType.ACTIVE);
    }

    @Test
    public void changeStatus() {
        StoryImpl story = new StoryImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, PriorityType.MEDIUM);
        story.changeStatus(StatusType.NOT_DONE);
    }

    @Test
    public void changePriority() {
        StoryImpl story = new StoryImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, PriorityType.MEDIUM);
        story.changePriority(PriorityType.HIGH);
    }

    @Test
    public void changeSize() {
        StoryImpl story = new StoryImpl(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, PriorityType.MEDIUM);
        story.changeSize(SizeType.LARGE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenPriorityIsNotRight() {
        FactoryWIMImpl factory = new FactoryWIMImpl();
        Story story = factory.createStory(Constants.SAMPLE_TITLE, Constants.SAMPLE_DESCRIPTION, "H");
    }
}
