import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.factories.FactoryWIMImpl;
import com.company.core.providers.CommandParser;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

public class AssignCommandTest {
    private FactoryWIMImpl factory;
    private EngineImpl engine;
    private CommandParser commandParser;

    @Before
    public void beforeTestInitializeFactoryEngineParser() {
        factory = new FactoryWIMImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory, engine);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandAssignNoParameters() {
        Command testCommand = commandParser.parseCommand(Constants.ASSIGN_NO_PARAMETERS);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.ASSIGN_NO_PARAMETERS);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandUnassignNoParameters() {
        Command testCommand = commandParser.parseCommand(Constants.UNASSIGN_NO_PARAMETERS);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.UNASSIGN_NO_PARAMETERS);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandUnassignNotValidID() {
        Command testCommand = commandParser.parseCommand(Constants.UNASSIGN_NOT_VALID_WORK_ITEM_ID);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.UNASSIGN_NOT_VALID_WORK_ITEM_ID);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandAssignNotExistingWorkItem() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN);
        Map<String, String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommand = commandParser.parseCommand(Constants.ASSIGN_NOT_EXISTING_WORK_ITEM);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.ASSIGN_NOT_EXISTING_WORK_ITEM);
        String result = testCommand.execute(testParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void commandUnassignNotExistingWorkItem() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN);
        Map<String, String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommand = commandParser.parseCommand(Constants.UNASSIGN_NOT_EXISTING_WORK_ITEM);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.UNASSIGN_NOT_EXISTING_WORK_ITEM);
        String result = testCommand.execute(testParameters);
    }
}


