import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.factories.FactoryWIMImpl;
import com.company.core.providers.CommandParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

public class ListCommandTest {
    private FactoryWIMImpl factory;
    private EngineImpl engine;
    private CommandParser commandParser;

    @Before
    public void beforeTestInitializeFactoryEngineParser() {
        factory = new FactoryWIMImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory, engine);
    }

    @Test
    public void listAllPeople() {
        Command testCommand1 = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN);
        Map<String,String> testParameters1 = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN);
        String result1 = testCommand1.execute(testParameters1);

        Command testCommand2 = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN2);
        Map<String,String> testParameters2 = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN2);
        String result2 = testCommand2.execute(testParameters2);


        Command testCommand = commandParser.parseCommand(Constants.LIST_ALL_PEOPLE);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_ALL_PEOPLE);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.LIST_ALL_PEOPLE_RESULT,result);
    }

    @Test
    public void listAllWorkItemWhenNoItems() {
        Command testCommand = commandParser.parseCommand(Constants.LIST_ALL);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_ALL);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals("", result);
    }

    @Test
    public void listAllWorkItems() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String,String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandBug = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String,String> testParametersBug = commandParser.parseParameters(Constants.CREATE_BUG);
        String resultBug = testCommandBug.execute(testParametersBug);

        Command testCommandFeedback1 = commandParser.parseCommand(Constants.CREATE_FEEDBACK);
        Map<String,String> testParametersFeedback1 = commandParser.parseParameters(Constants.CREATE_FEEDBACK);
        String resultFeedback1 = testCommandFeedback1.execute(testParametersFeedback1);

        Command testCommand = commandParser.parseCommand(Constants.LIST_ALL);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_ALL);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.LIST_ALL_RESULT, result);

    }

    @Test
    public void listAllFeedbacksEmpty() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String,String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandBug = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String,String> testParametersBug = commandParser.parseParameters(Constants.CREATE_BUG);
        String resultBug = testCommandBug.execute(testParametersBug);

        Command testCommand = commandParser.parseCommand(Constants.LIST_ALL_FEEDBACKS);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_ALL_FEEDBACKS);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals("", result);
    }

    @Test
    public void listAllFeedbacks() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String,String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandFeedback1 = commandParser.parseCommand(Constants.CREATE_FEEDBACK);
        Map<String,String> testParametersFeedback1 = commandParser.parseParameters(Constants.CREATE_FEEDBACK);
        String resultFeedback1 = testCommandFeedback1.execute(testParametersFeedback1);

        Command testCommandFeedback2 = commandParser.parseCommand(Constants.CREATE_FEEDBACK2);
        Map<String,String> testParametersFeedback2 = commandParser.parseParameters(Constants.CREATE_FEEDBACK2);
        String resultFeedback2 = testCommandFeedback2.execute(testParametersFeedback2);

        Command testCommand = commandParser.parseCommand(Constants.LIST_ALL_FEEDBACKS);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_ALL_FEEDBACKS);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.LIST_ALL_FEEDBACKS_RESULT, result);
    }

    @Test
    public void listAllStoryEmpty() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String,String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandBug = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String,String> testParametersBug = commandParser.parseParameters(Constants.CREATE_BUG);
        String resultBug = testCommandBug.execute(testParametersBug);

        Command testCommand = commandParser.parseCommand(Constants.LIST_ALL_STORIES);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_ALL_STORIES);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals("", result);
    }

    @Test
    public void listAllStories() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String,String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandStory1 = commandParser.parseCommand(Constants.CREATE_STORY);
        Map<String,String> testParametersStory1= commandParser.parseParameters(Constants.CREATE_STORY);
        String resultStory1 = testCommandStory1.execute(testParametersStory1);

        Command testCommandStory2 = commandParser.parseCommand(Constants.CREATE_STORY2);
        Map<String,String> testParametersStory2 = commandParser.parseParameters(Constants.CREATE_STORY2);
        String resultStory2 = testCommandStory2.execute(testParametersStory2);

        Command testCommandBug = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String,String> testParametersBug = commandParser.parseParameters(Constants.CREATE_BUG);
        String resultBug = testCommandBug.execute(testParametersBug);

        Command testCommand = commandParser.parseCommand(Constants.LIST_ALL_STORIES);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_ALL_STORIES);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.LIST_ALL_STORIES_RESULT, result);
    }

    @Test
    public void listAllBugsEmpty() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String,String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandStory1 = commandParser.parseCommand(Constants.CREATE_STORY);
        Map<String,String> testParametersStory1= commandParser.parseParameters(Constants.CREATE_STORY);
        String resultStory1 = testCommandStory1.execute(testParametersStory1);

        Command testCommand = commandParser.parseCommand(Constants.LIST_ALL_BUGS);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_ALL_BUGS);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals("", result);
    }

    @Test
    public void listAllBugs() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String,String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandBug = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String,String> testParametersBug = commandParser.parseParameters(Constants.CREATE_BUG);
        String resultBug = testCommandBug.execute(testParametersBug);

        Command testCommandFeedback2 = commandParser.parseCommand(Constants.CREATE_FEEDBACK2);
        Map<String,String> testParametersFeedback2 = commandParser.parseParameters(Constants.CREATE_FEEDBACK2);
        String resultFeedback2 = testCommandFeedback2.execute(testParametersFeedback2);

        Command testCommand = commandParser.parseCommand(Constants.LIST_ALL_BUGS);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_ALL_BUGS);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.LIST_ALL_BUGS_RESULT, result);
    }

    @Test
    public void listSortedBySeverity() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String,String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandBug2 = commandParser.parseCommand(Constants.CREATE_BUG2);
        Map<String,String> testParametersBug2 = commandParser.parseParameters(Constants.CREATE_BUG2);
        String resultBug2 = testCommandBug2.execute(testParametersBug2);

        Command testCommandBug = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String,String> testParametersBug = commandParser.parseParameters(Constants.CREATE_BUG);
        String resultBug = testCommandBug.execute(testParametersBug);

        Command testCommandFeedback2 = commandParser.parseCommand(Constants.CREATE_FEEDBACK2);
        Map<String,String> testParametersFeedback2 = commandParser.parseParameters(Constants.CREATE_FEEDBACK2);
        String resultFeedback2 = testCommandFeedback2.execute(testParametersFeedback2);

        Command testCommand = commandParser.parseCommand(Constants.LIST_SORTED_SEVERITY);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_SORTED_SEVERITY);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.LIST_SORTED_RESULT, result);
    }

    @Test
    public void listSortedByStatus() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String,String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandBug = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String,String> testParametersBug = commandParser.parseParameters(Constants.CREATE_BUG);
        String resultBug = testCommandBug.execute(testParametersBug);

        Command testCommandStory1 = commandParser.parseCommand(Constants.CREATE_STORY);
        Map<String,String> testParametersStory1= commandParser.parseParameters(Constants.CREATE_STORY);
        String resultStory1 = testCommandStory1.execute(testParametersStory1);

        Command testCommand = commandParser.parseCommand(Constants.LIST_STATUS_ACTIVE);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_STATUS_ACTIVE);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.LIST_STATUS_RESULT, result);
    }

    @Test
    public void listSortedByTitle() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String,String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandBug2 = commandParser.parseCommand(Constants.CREATE_BUG2);
        Map<String,String> testParametersBug2 = commandParser.parseParameters(Constants.CREATE_BUG2);
        String resultBug2 = testCommandBug2.execute(testParametersBug2);

        Command testCommandBug3 = commandParser.parseCommand(Constants.CREATE_BUG3);
        Map<String,String> testParametersBug3 = commandParser.parseParameters(Constants.CREATE_BUG3);
        String resultBug3 = testCommandBug3.execute(testParametersBug3);

        Command testCommandStory1 = commandParser.parseCommand(Constants.CREATE_STORY);
        Map<String,String> testParametersStory1= commandParser.parseParameters(Constants.CREATE_STORY);
        String resultStory1 = testCommandStory1.execute(testParametersStory1);

        Command testCommandBug = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String,String> testParametersBug = commandParser.parseParameters(Constants.CREATE_BUG);
        String resultBug = testCommandBug.execute(testParametersBug);

        Command testCommand = commandParser.parseCommand(Constants.LIST_SORTED_BY_TITLE);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_SORTED_BY_TITLE);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.LIST_SORTED_BY_TITLE_RESULT, result);
    }

    @Test
    public void listSortedByRating() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String,String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String,String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandBug = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String,String> testParametersBug = commandParser.parseParameters(Constants.CREATE_BUG);
        String resultBug = testCommandBug.execute(testParametersBug);

        Command testCommandFeedback2 = commandParser.parseCommand(Constants.CREATE_FEEDBACK2);
        Map<String,String> testParametersFeedback2 = commandParser.parseParameters(Constants.CREATE_FEEDBACK2);
        String resultFeedback2 = testCommandFeedback2.execute(testParametersFeedback2);

        Command testCommandFeedback4 = commandParser.parseCommand(Constants.CREATE_FEEDBACK4);
        Map<String,String> testParametersFeedback4 = commandParser.parseParameters(Constants.CREATE_FEEDBACK4);
        String resultFeedback4 = testCommandFeedback4.execute(testParametersFeedback4);

        Command testCommandFeedback3 = commandParser.parseCommand(Constants.CREATE_FEEDBACK3);
        Map<String,String> testParametersFeedback3 = commandParser.parseParameters(Constants.CREATE_FEEDBACK3);
        String resultFeedback3 = testCommandFeedback3.execute(testParametersFeedback3);

        Command testCommand = commandParser.parseCommand(Constants.LIST_SORTED_RATING);
        Map<String,String> testParameters = commandParser.parseParameters(Constants.LIST_SORTED_RATING);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals(Constants.LIST_SORTED_RATING_RESULT, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void listSortedByAssigneeNotExisting(){
        Command testCommand = commandParser.parseCommand(Constants.LIST_SORTED_BY_ASSIGNEE);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.LIST_SORTED_BY_ASSIGNEE);
        String result = testCommand.execute(testParameters);
        Assert.assertEquals("", result);
    }

    @Test
    public void listSortedByAssignee(){
        Command testCommand1 = commandParser.parseCommand(Constants.CREATE_PERSON_MARTIN);
        Map<String,String> testParameters1 = commandParser.parseParameters(Constants.CREATE_PERSON_MARTIN);
        String result1 = testCommand1.execute(testParameters1);

        Command testCommand = commandParser.parseCommand(Constants.LIST_SORTED_BY_ASSIGNEE);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.LIST_SORTED_BY_ASSIGNEE);
        String result = testCommand.execute(testParameters);
    }


    @Test(expected = IllegalArgumentException.class)
    public void listSortedByUnknownType() {
        Command testCommandTeam = commandParser.parseCommand(Constants.CREATE_TEAM_AWESOME);
        Map<String, String> testParametersTeam = commandParser.parseParameters(Constants.CREATE_TEAM_AWESOME);
        String resultTeam = testCommandTeam.execute(testParametersTeam);

        Command testCommandBoard = commandParser.parseCommand(Constants.CREATE_BOARD_MY_BOARD);
        Map<String, String> testParametersBoard = commandParser.parseParameters(Constants.CREATE_BOARD_MY_BOARD);
        String resultBoard = testCommandBoard.execute(testParametersBoard);

        Command testCommandBug2 = commandParser.parseCommand(Constants.CREATE_BUG2);
        Map<String, String> testParametersBug2 = commandParser.parseParameters(Constants.CREATE_BUG2);
        String resultBug2 = testCommandBug2.execute(testParametersBug2);

        Command testCommandBug3 = commandParser.parseCommand(Constants.CREATE_BUG3);
        Map<String, String> testParametersBug3 = commandParser.parseParameters(Constants.CREATE_BUG3);
        String resultBug3 = testCommandBug3.execute(testParametersBug3);

        Command testCommandStory1 = commandParser.parseCommand(Constants.CREATE_STORY);
        Map<String, String> testParametersStory1 = commandParser.parseParameters(Constants.CREATE_STORY);
        String resultStory1 = testCommandStory1.execute(testParametersStory1);

        Command testCommandBug = commandParser.parseCommand(Constants.CREATE_BUG);
        Map<String, String> testParametersBug = commandParser.parseParameters(Constants.CREATE_BUG);
        String resultBug = testCommandBug.execute(testParametersBug);

        Command testCommand = commandParser.parseCommand(Constants.LIST_SORTED_BY_UNKNOWN_TYPE);
        Map<String, String> testParameters = commandParser.parseParameters(Constants.LIST_SORTED_BY_UNKNOWN_TYPE);
        String result = testCommand.execute(testParameters);
    }
}
