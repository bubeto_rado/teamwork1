# TeamWork1
:computer: :wink:  
:beetle: :four_leaf_clover:  

Trello Board
https://trello.com/b/CmyrlkRD/work-item-management-project

## Commands
create -person Bubeto  
create -person asd  
create -person Bubeto  
create -person Martin  
create -person Mimeto  

create -team QA  
create -team Malinka  
create -team devs  
create -team devs  
create -team  

create -team Malinka  
create -board Malinka DailyTasks  
create -board Kapinka Supertasks  
create -board Malinka ImportantTasks  
create -board Malinka    
create -board Malinka 231  

### correct input data bug
create -team Malinka  
create -board Malinka DailyTasks  
create -bug -board Malinka DailyTasks -title Big Bug CodeEater -description An evil bug that eats junior's code  
create -bug -board Malinka DailyTasks -title Icecream Monster! -description Small bug looking for icecream! -steps first you start engine #second engine blow#thirth game over  

### incorrect input data bug
create -team Malinka  
create -board Malinka DailyTasks  
create -bug -title Big bad bug  
create -bug -title big bad bug -board malinka DailyTasks  
create -bug -title big bad bug -board QA DailyTasks -description s  
create -bug -title BIG BAD BUG -board Malinka IncorrectBoard -description long long long description  
create -bug -title BIG BAD EVIL BUG -board Malinka DailyTasks -description short  

### correct input data assignee
create -team malinka  
create -person Bubeto  
add -person Bubeto malinka  
create -board malinka board1  
create -bug -board malinka board1 -title bugTitle111111 -description sssssssssssssssssssssssssssssss  
create -bug -board malinka board1 -title bugTitle22222 -description sssssssssssssssssssssssssssssss -severity Major  
create -bug -board malinka board1 -title bugTitle333333 -description sssssssssssssssssssssssssssssss -severity Critical  
assign -person Bubeto -workitem 1  
assign -person Bubeto -workitem 2  
assign -person Bubeto -workitem 3  
unassign -person Bubeto -workitem 1  

### incorrect input data assignee
create -team malinka  
create -person Bubeto   
add -person Bubeto malinka  
create -board malinka board1  
create -bug -board malinka board1 -title bugTitle111111 -description sssssssssssssssssssssssssssssss  
create -bug -board malinka board1 -title bugTitle22222 -description sssssssssssssssssssssssssssssss -severity Major  
create -bug -board malinka board1 -title bugTitle333333 -description sssssssssssssssssssssssssssssss -severity Critical  
assign -person Martin -workitem 1  
assign -person Superman -workitem 2  
assign -person Bubeto -workitem 22323232  
unassign -person Batman -workitem 1  

### correct input data story
create -person Martin  
create -person Bubeto  
create -team Malinka  
create -board Malinka DailyTasks  
create -story -title SomeTitle again -description super description -board Malinka DailyTasks  
create -story -title NextStory22222  -description super description -board Malinka DailyTasks -size Large  
create -story -title Long story for coders -description super borring descrption -board Malinka DailyTasks -size Medium -status InProgress  
show -workitem 2  

### correct input data feedback
create -person Martin  
create -person Bubeto  
create -person Martin  
create -team Malinka  
create -board Malinka DailyTasks  
create -feedback -title Good feedback  -description some description to type -board Malinka DailyTasks  
create -feedback -title Bad feedback  -description some description to type -board Malinka DailyTasks -rating 9  
create -feedback -title supe123123123r -description some description to type -board Malinka DailyTasks -rating 12  
show -workitem 3  

### incorrect input data feedback
create -person Martin  
create -person Bubeto  
create -person Martin  
create -team Malinka  
create -board Malinka DailyTasks  
create -feedback -title short  -description some description to type -board Malinka DailyTasks  
create -feedback -title Goooddd  -description some description to type -board Malinka no board   
create -feedback -title no description    -board Malinka DailyTasks  

### correct input data add person
create -person Martin      
create -person Bubeto       
create -team Malinka    
create -board Malinka DailyTasks  
add -person Bubeto Malinka  
add -person Martin Malinka  

### incorrect input data add person
create -person Martin      
create -person Bubeto       
create -team Malinka    
create -board Malinka DailyTasks  
add -person NOT EXISTING -board Malinka DailtTasks  
add -person Martin -board Malinka NOBOARD

### correct input data add comment  
create -team Malinka  
create -board Malinka board1    
create -bug -board Malinka board1 -title bugTitle111111 -description sssssssssssssssssssssssssssssss  
add -comment 1 JAVA You shall not pass !  
add -comment 1 Boss I like it! 
show -workitem 1  

### incorrect input data add comment
create -team Malinka  
create -board Malinka board1    
create -bug -board Malinka board1 -title bugTitle111111 -description sssssssssssssssssssssssssssssss  
add -comment 2 Java You shall not pass !    


### list commands
create -person Bubeto 
create -person Martin  
create -person Galin1   
create -team Kapinka  
create -team IceCream
create -team Malinka  
add -person Bubeto Malinka  
add -person Galin1 Malinka  
add -person Martin Kapinka  
assign -workitem 1 -person Bubeto  
assigne -workitem 3 -person Bubeto  
create -board IceCream BIG BOARD  
create -board Malinka DailyTasks  
create -bug -board Malinka DailyTasks -title Big Bug CodeEater -description An evil bug that eats junior's code  
create -bug -board Malinka DailyTasks -title Icecream Monster! -description Small bug looking for icecream! -steps first you start engine #second engine blow#thirth game over  
create -story -title SomeTitle again -description super description -board Malinka DailyTasks  
create -story -title NextStory22222  -description super description -board Malinka DailyTasks -size Large  
create -story -title Long story for coders -description super borring descrption -board Malinka DailyTasks -size Medium -status InProgress  
create -feedback -title Good feedback  -description some description to type -board Malinka DailyTasks  
create -feedback -title Bad feedback  -description some description to type -board Malinka DailyTasks -rating 9  
create -feedback -title supe123123123r -description some description to type -board Malinka DailyTasks -rating 12  
list -bugs  
list -stories  
list -feedbacks  
list -people    
list -board Malinka DailyTasks  
list -team Malinka  
list -teams  
assign -workitem 1 -person Bubeto    
assign -workitem 4 -person Bubeto  
list -bugs -assignee Bubeto 
list -stories -assignee Bubeto  
list -bugs -status Active  
list -stories -sort size  
list -sort rating  
list -sort severity  

### some show commands

create -person Bubeto  
create -person Martin   
create -person Galin1   
create -team Kapinka  
create -team IceCream  
create -team Malinka  
add -person Bubeto Malinka  
add -person Galin1 Malinka  
add -person Martin Kapinka  
assign -workitem 1 -person Bubeto  
assigne -workitem 3 -person Bubeto  
create -board IceCream BIG BOARD  
create -board Malinka DailyTasks  
create -bug -board Malinka DailyTasks -title Big Bug CodeEater -description An evil bug that eats junior's code  
create -bug -board Malinka DailyTasks -title Icecream Monster! -description Small bug looking for icecream! -steps first you start engine #second engine blow#thirth game over  
create -story -title SomeTitle again -description super description -board Malinka DailyTasks  
create -story -title NextStory22222  -description super description -board Malinka DailyTasks -size Large    
create -story -title Long story for coders -description super borring descrption -board Malinka DailyTasks -size Medium -status InProgress  
create -feedback -title Good feedback  -description some description to type -board Malinka DailyTasks  
create -feedback -title Bad feedback  -description some description to type -board Malinka DailyTasks -rating 9  
create -feedback -title supe123123123r -description some description to type -board Malinka DailyTasks -rating 12  
show -members Malinka  
show -boards Malinka  
create -board Malinka Gelato  
show -boards Malinka  
show -board Malinka Gelato  
show -boards Malinka  


### some change commands

create -person Bubeto  
create -person Martin   
create -person Galin1   
create -team Kapinka  
create -team IceCream  
create -team Malinka  
add -person Bubeto Malinka  
add -person Galin1 Malinka  
add -person Martin Kapinka  
assign -workitem 1 -person Bubeto  
assigne -workitem 3 -person Bubeto  
create -board IceCream BIG BOARD  
create -board Malinka DailyTasks  
create -bug -board Malinka DailyTasks -title Big Bug CodeEater -description An evil bug that eats junior's code  
create -bug -board Malinka DailyTasks -title Icecream Monster! -description Small bug looking for icecream! -steps first you start engine #second engine blow#thirth game over  
create -story -title SomeTitle again -description super description -board Malinka DailyTasks  
create -story -title NextStory22222  -description super description -board Malinka DailyTasks -size Large    
create -story -title Long story for coders -description super borring descrption -board Malinka DailyTasks -size Medium -status InProgress  
create -feedback -title Good feedback  -description some description to type -board Malinka DailyTasks  
create -feedback -title Bad feedback  -description some description to type -board Malinka DailyTasks -rating 9  
create -feedback -title supe123123123r -description some description to type -board Malinka DailyTasks -rating 12  
change -workitem 1 -status fixed  
change -workitem 1 -status sfdgdf  
show -workitem 1  
change -workitem 1 -severity Major  
show -workitem 1  
change -workitem 1 -severity Minor  
show -workitem 1  
show -workitem 4  
change -workitem 4 -status NotDone  
show -workitem 4  
change workitem 4 -size Medium  
show -workitem 4  
change -workitem 6 -rating 500  
show -workitem 6  
change workitem 6 -rating 250  
show -workitem 6  
change -workite 1 -priority Low
show -workitem 1  
change -workitem 1 -priority High  
show -workitem 1  





